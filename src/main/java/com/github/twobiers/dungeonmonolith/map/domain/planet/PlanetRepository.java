package com.github.twobiers.dungeonmonolith.map.domain.planet;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetRepository extends JpaRepository<Planet, UUID> {

}
