package com.github.twobiers.dungeonmonolith.robot.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class GameworldStatusChangedEventDto(
  val id: UUID,
  val status: GameworldStatus
)
