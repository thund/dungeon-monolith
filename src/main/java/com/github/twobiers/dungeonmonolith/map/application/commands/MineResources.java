package com.github.twobiers.dungeonmonolith.map.application.commands;

import com.github.twobiers.dungeonmonolith.map.application.commands.dto.MineResourcesDto;
import com.github.twobiers.dungeonmonolith.map.application.commands.dto.MineResourcesResponseDto;
import com.github.twobiers.dungeonmonolith.map.domain.planet.PlanetRepository;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class MineResources {
  private final PlanetRepository planetRepository;

  @Transactional
  public MineResourcesResponseDto mine(UUID planetId, MineResourcesDto dto) {
    var planet = planetRepository.findById(planetId).orElseThrow(() -> new ResponseStatusException(
        HttpStatus.NOT_FOUND));
    var resource = planet.getResource();
    var amountToMine = dto.amountToMine();
    var amountBeforeMining = resource != null ? resource.getCurrentAmount() : 0;

    planet.mine(amountToMine);
    assert resource != null;

    var minedAmount = amountBeforeMining - resource.getCurrentAmount();
    planetRepository.save(planet);

    return new MineResourcesResponseDto(minedAmount);
  }
}
