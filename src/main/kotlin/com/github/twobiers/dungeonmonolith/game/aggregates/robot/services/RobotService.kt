package com.github.twobiers.dungeonmonolith.game.aggregates.robot.services

import com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain.GameRobot
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain.RobotAlreadyExistsException
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain.RobotNotFoundException
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.repositories.GameRobotRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import jakarta.transaction.Transactional

@Service
class RobotService @Autowired constructor(
    private val robotRepository: GameRobotRepository
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @Transactional(rollbackOn = [Exception::class])
    fun newRobot(robotId: UUID, playerId: UUID) {
        if (robotRepository.existsById(robotId)) {
            logger.warn("Failed to create new robot. Robot does already exist. [robotId=$robotId]")
            throw RobotAlreadyExistsException("Failed to create new robot. Robot does already exist.")
        }

        val newRobot = GameRobot(robotId, playerId)
        robotRepository.save(newRobot)
        logger.trace("Robot created. [robotId=${robotId}, playerId=${playerId}]")
    }

    @Transactional(rollbackOn = [Exception::class])
    fun destroyRobot(robotId: UUID) {
        val robot = robotRepository.findById(robotId)
            .orElseThrow { RobotNotFoundException("Failed to destroy robot. Robot not found.") }

        robot.destroyRobot()
        robotRepository.save(robot)
        logger.trace("Robot destroyed. [robotId=$robotId]")
    }
}
