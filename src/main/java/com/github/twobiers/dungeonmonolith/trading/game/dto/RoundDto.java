package com.github.twobiers.dungeonmonolith.trading.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RoundDto(
    UUID gameId,
    UUID roundId,
    int roundNumber
) {}
