package com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain

class RobotNotFoundException(message: String?) : Exception(message) {
}
