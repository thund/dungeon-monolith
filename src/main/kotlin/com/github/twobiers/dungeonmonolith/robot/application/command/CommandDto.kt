package com.github.twobiers.dungeonmonolith.robot.application.command

data class CommandDto(
  val commands: List<RobotCommand>
)
