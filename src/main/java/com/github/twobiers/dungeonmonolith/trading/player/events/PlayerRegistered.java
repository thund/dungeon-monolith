package com.github.twobiers.dungeonmonolith.trading.player.events;

import java.util.UUID;

public record PlayerRegistered(
    UUID id,
    UUID gameId
) {

}
