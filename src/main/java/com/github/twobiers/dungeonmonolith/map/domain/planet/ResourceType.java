package com.github.twobiers.dungeonmonolith.map.domain.planet;

public enum ResourceType {
  COAL,
  IRON,
  GEM,
  GOLD,
  PLATIN
}
