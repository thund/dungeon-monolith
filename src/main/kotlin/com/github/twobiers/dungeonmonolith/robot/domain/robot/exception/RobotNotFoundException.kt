package com.github.twobiers.dungeonmonolith.robot.domain.robot.exception

import com.github.twobiers.dungeonmonolith.robot.application.core.FailureException

class RobotNotFoundException(s: String) : FailureException(s)
