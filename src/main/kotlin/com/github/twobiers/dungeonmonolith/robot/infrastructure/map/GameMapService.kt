package com.github.twobiers.dungeonmonolith.robot.infrastructure.map

import java.util.*

interface GameMapService {
  fun mine(planetId: UUID, amount: Int): Int
}
