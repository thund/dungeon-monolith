package com.github.twobiers.dungeonmonolith.game.aggregates.command.domain

class CommandParsingException(override val message: String) : Exception(message) {
}
