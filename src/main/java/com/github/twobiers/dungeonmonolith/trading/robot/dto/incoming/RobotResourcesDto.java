package com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public record RobotResourcesDto(
    int coal,
    int iron,
    int gem,
    int gold,
    int platin
) {

}
