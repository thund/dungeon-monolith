package com.github.twobiers.dungeonmonolith.robot.domain.planet

enum class PlanetType(val stringValue: String) {
  DEFAULT("default"),
  SPACE_STATION("spacestation")
}
