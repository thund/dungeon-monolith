package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

enum class GameStatus {
    CREATED,
    GAME_RUNNING,
    GAME_FINISHED
}
