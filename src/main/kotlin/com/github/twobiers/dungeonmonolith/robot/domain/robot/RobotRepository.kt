package com.github.twobiers.dungeonmonolith.robot.domain.robot

import com.github.twobiers.dungeonmonolith.robot.domain.robot.exception.RobotNotFoundException
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

@JvmDefaultWithCompatibility
interface RobotRepository : CrudRepository<Robot, UUID> {

  @Query("select r from Robot r where r.alive = true")
  fun findAllAlive(): List<Robot>

  @Query("select r from Robot r where r.alive = false and r.planet.planetId = ?1")
  fun findDeadRobotsOnPlanet(planetId: UUID): List<Robot>

  @Query("select r from Robot r where r.planet.planetId = ?1")
  fun findAllRobotsOnPlanet(planetId: UUID): List<Robot>

  @Query("select r from Robot r where r.planet.planetId in ?1")
  fun findAllRobotsOnPlanetIn(planetIds: List<UUID>): List<Robot>

  @Query("select r from Robot r where r.player = ?1 and r.planet.planetId = ?2")
  fun findAllRobotsOfPlayerOnPlanet(playerId: UUID, planetId: UUID): List<Robot>

  fun findAllByPlayer(playerId: UUID): List<Robot>

  /**
   * @throws RobotNotFoundException
   */
  fun findByIdOrThrow(robotId: UUID): Robot =
    findById(robotId).orElseThrow { RobotNotFoundException("Robot with id $robotId not found") }

  @Modifying
  @Query("delete from Robot r where r in (select r1 from Robot r1 join r1.planet p where p.gameWorldId = ?1)")
  fun deleteAllRobotsInGameworld(gameworldId: UUID)
}
