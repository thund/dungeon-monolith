package com.github.twobiers.dungeonmonolith.map.domain.gameworld;

public enum GameworldStatus {
  ACTIVE, INACTIVE
}
