package com.github.twobiers.dungeonmonolith.game.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto.CommandRequestDto
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
class GameJacksonConfig {

    @Bean
    fun commandRequestReader(mapper: ObjectMapper): ObjectReader = mapper.readerFor(
        CommandRequestDto::class.java
    )
        .with(
            DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
            DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
        )
}
