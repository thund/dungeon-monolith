package com.github.twobiers.dungeonmonolith.trading.command.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.twobiers.dungeonmonolith.trading.command.CommandType;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record CommandDto(
    UUID playerId,
    CommandPayloadDto payload
) {
    public record CommandPayloadDto(
        CommandType commandType,
        Integer amount,
        String itemName,
        UUID robotId
    ) {}
}
