package com.github.twobiers.dungeonmonolith.trading.game.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.github.twobiers.dungeonmonolith.trading.game.GameStatus;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record GameStatusDto(
    UUID gameId,
    GameStatus status
) {}
