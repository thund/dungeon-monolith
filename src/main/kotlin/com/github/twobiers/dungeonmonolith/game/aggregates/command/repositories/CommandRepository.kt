package com.github.twobiers.dungeonmonolith.game.aggregates.command.repositories

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandType
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.Round
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CommandRepository : JpaRepository<Command, UUID> {

    @Query("SELECT c FROM Command c join c.round r where r.roundId = ?1 and c.commandType = ?2")
    fun findAllCommandsByRoundAndCommandType(round: UUID, commandType: CommandType): List<Command>

    fun findAllCommandsByRound(round: Round): List<Command>

    @Query(
        """
        select c from Command c 
          left join c.round r
          left join c.player p
          left join c.robot ro
          where r.roundId = ?1 
          and p.playerId = ?2
          and ro.robotId = ?3
         """
    )
    fun findCommandByRoundAndPlayerAndRobot(round: UUID, player: UUID, robot: UUID?): Command?

    fun findAllByRoundGameGameIdAndRoundRoundNumber(gameId: UUID, roundNumber: Int): List<Command>
}
