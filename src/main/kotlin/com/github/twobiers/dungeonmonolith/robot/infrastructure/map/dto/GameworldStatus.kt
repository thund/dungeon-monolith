package com.github.twobiers.dungeonmonolith.robot.infrastructure.map.dto

import java.util.UUID

enum class GameworldStatus {
  ACTIVE, INACTIVE
}
