package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import java.util.UUID

data class RobotResourceMinedIntegrationEvent(
  val robotId: UUID,
  val minedResource: ResourceType,
  val minedAmount: Int,
  val resourceInventory: Map<ResourceType, Int>
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot, minedResource: ResourceType, minedAmount: Int) =
      RobotResourceMinedIntegrationEvent(
        robotId = robot.id,
        minedResource = minedResource,
        minedAmount = minedAmount,
        resourceInventory = robot.inventory.resources
      )
  }

  override fun key(): String = robotId.toString()
}
