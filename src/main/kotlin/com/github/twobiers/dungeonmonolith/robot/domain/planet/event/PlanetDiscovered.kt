package com.github.twobiers.dungeonmonolith.robot.domain.planet.event

import com.github.twobiers.dungeonmonolith.robot.domain.planet.RobotPlanet

data class PlanetDiscovered(
  val planet: RobotPlanet
) : PlanetDomainEvent
