package com.github.twobiers.dungeonmonolith.robot.domain.planet

import com.fasterxml.jackson.annotation.JsonProperty

enum class ResourceType(val requiredMiningLevel: Int) {
  COAL(0),
  IRON(1),
  GEM(2),
  GOLD(3),
  PLATIN(4);

  override fun toString(): String {
    return when (this) {
      COAL -> "coal"
      IRON -> "iron"
      GEM -> "gem"
      GOLD -> "gold"
      PLATIN -> "platin"
    }
  }
}
