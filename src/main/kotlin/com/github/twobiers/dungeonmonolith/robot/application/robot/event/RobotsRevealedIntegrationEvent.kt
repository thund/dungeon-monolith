package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import java.util.*

data class RobotsRevealedIntegrationEvent(
  val robots: List<RobotRevealedDto>
) : RobotIntegrationEvent {
  companion object {
    data class RobotRevealedDto(
      val robotId: UUID,
      val planetId: UUID,
      val playerNotion: String,
      val health: Int,
      val energy: Int,
      val levels: RobotLevels,
    )
    data class RobotLevels(
      var healthLevel: Int,
      var damageLevel: Int,
      var miningSpeedLevel: Int,
      val miningLevel: Int,
      val energyLevel: Int,
      val energyRegenLevel: Int,
      val storageLevel: Int
    )

    fun build(robots: List<Robot>): RobotsRevealedIntegrationEvent {
      return RobotsRevealedIntegrationEvent(
        robots.map { robot ->
          RobotRevealedDto(
            robot.id,
            robot.planet.planetId,
            robot.player.toString().substringBefore('-'),
            robot.health,
            robot.energy,
            RobotLevels(
              robot.healthLevel,
              robot.damageLevel,
              robot.miningSpeedLevel,
              robot.miningLevel,
              robot.energyLevel,
              robot.energyRegenLevel,
              robot.inventory.storageLevel
            )
          )
        }
      )
    }
  }

  override fun key(): String = ""
}
