package com.github.twobiers.dungeonmonolith.game.aggregates.game.events

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.game.aggregates.core.Event
import com.github.twobiers.dungeonmonolith.game.aggregates.core.EventBuilder
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.GameStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class GameStatusEventBuilder @Autowired constructor(
) : EventBuilder {
    private val topic: String = "status"
    private val eventType: String = "game-status"
    private val version: Int = 1

    companion object {
        val objectMapper: ObjectMapper = ObjectMapper().findAndRegisterModules()
    }

    override fun deserializedEvent(serialized: String): Event {
        return objectMapper.readValue(serialized, GameStatusEvent::class.java)
    }

    fun makeGameStatusEvent(transactionId: UUID, gameId: UUID, gameStatus: GameStatus, gameworldId: UUID? = null): GameStatusEvent {
        return GameStatusEvent(
            id = UUID.randomUUID(),
            gameworldId = gameworldId,
            transactionId = transactionId,
            occurredAt = Instant.now(),
            eventName = eventType,
            topic = topic,
            version = version,
            gameId = gameId,
            gameStatus = gameStatus
        )
    }
}
