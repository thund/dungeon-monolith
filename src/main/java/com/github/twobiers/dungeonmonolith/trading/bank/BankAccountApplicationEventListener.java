package com.github.twobiers.dungeonmonolith.trading.bank;

import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountDepositRequested;
import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountWithdrawalRequested;
import com.github.twobiers.dungeonmonolith.trading.game.event.GameStarted;
import com.github.twobiers.dungeonmonolith.trading.game.event.GameStopped;
import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@Slf4j
public class BankAccountApplicationEventListener {
    private final BankAccountService bankAccountService;

    public BankAccountApplicationEventListener(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @EventListener(GameStopped.class)
    public void onGameStopped(GameStopped gameStopped) {
        log.trace("Game {} stopped, resetting all bank accounts", gameStopped.id());
        this.bankAccountService.resetAllBankAccounts();
    }

    @EventListener(GameStarted.class)
    public void onGameStarted(GameStarted gameStarted) {
        log.trace("Game {} started, initializing bank account for players", gameStarted.id());

        gameStarted.participatingPlayers().forEach(playerId -> {
            this.bankAccountService.initializeForPlayer(playerId, BigDecimal.valueOf(500));
        });
    }

    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void onDepositRequested(BankAccountDepositRequested depositRequested) {
        var bankAccount = this.bankAccountService.findByPlayerId(depositRequested.playerId());
        log.trace("Requested Deposit of {} for Player {}. Current Balance: {}", depositRequested.depositAmount(), bankAccount.getPlayer().getName(), bankAccount.getBalance());
        bankAccount.deposit(depositRequested.depositAmount());
        bankAccountService.save(bankAccount);
    }

    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void onWithdrawalRequested(BankAccountWithdrawalRequested withdrawalRequested) {
        var bankAccount = this.bankAccountService.findByPlayerId(withdrawalRequested.playerId());
        log.trace("Requested Withdrawal of {} for Player {}. Current Balance: {}", withdrawalRequested.withdrawalAmount(), bankAccount.getPlayer().getName(), bankAccount.getBalance());
        bankAccount.withdraw(withdrawalRequested.withdrawalAmount());
        bankAccountService.save(bankAccount);
    }
}
