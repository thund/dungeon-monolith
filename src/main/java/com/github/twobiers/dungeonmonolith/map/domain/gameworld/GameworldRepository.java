package com.github.twobiers.dungeonmonolith.map.domain.gameworld;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GameworldRepository extends JpaRepository<Gameworld, UUID> {

  @Query("select g from Gameworld g where g.status = 'ACTIVE'")
  Optional<Gameworld> findActive();

  @Query("""
    select g from Gameworld g where g.status = 'ACTIVE'
   """)
  List<Gameworld> findAllActive();

  @Query("""
    select g from Gameworld g join g.mapGrid mg join mg.planets p where p.id = ?1
   """)
  Gameworld getGameworldOfPlanet(UUID planetId);
}
