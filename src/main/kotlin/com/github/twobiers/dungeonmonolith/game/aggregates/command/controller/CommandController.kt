package com.github.twobiers.dungeonmonolith.game.aggregates.command.controller

import com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto.CommandRequestDto
import com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto.CommandResponseDto
import com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto.RoundCommandsResponseDto
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.command.repositories.CommandRepository
import com.github.twobiers.dungeonmonolith.game.aggregates.command.services.CommandService
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.RoundNotFoundException
import com.github.twobiers.dungeonmonolith.game.aggregates.game.repositories.RoundRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import jakarta.transaction.Transactional

@RestController
class CommandController @Autowired constructor(
    private val commandService: CommandService,
    private val commandRepository: CommandRepository,
    private val roundRepository: RoundRepository,
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @PostMapping("/commands", consumes = ["application/json"], produces = ["application/json"])
    fun createNewCommand(@RequestBody requestBody: CommandRequestDto): ResponseEntity<CommandResponseDto> {
        val transactionId = commandService.createNewCommand(
            requestBody.gameId, requestBody.playerId, requestBody.robotId, requestBody.commandType, requestBody
        )
        val responseBody = CommandResponseDto(transactionId)

        return ResponseEntity(responseBody, HttpStatus.CREATED)
    }

    @Transactional(rollbackOn = [Exception::class])
    @GetMapping("/commands", produces = ["application/json"])
    fun getAllRoundCommands(@RequestParam(name = "gameId") gameId: UUID, @RequestParam(name = "roundNumber") roundNumber: Int):
            ResponseEntity<RoundCommandsResponseDto>
    {
        val round = roundRepository.findRoundByGame_GameIdAndRoundNumber(gameId, roundNumber)
                .orElseThrow { RoundNotFoundException("Round Number $roundNumber of game '$gameId' not found.") }

        val commands: List<Command> = commandRepository.findAllByRoundGameGameIdAndRoundRoundNumber(gameId, roundNumber)
        val responseDto = RoundCommandsResponseDto(round, commands)
        return ResponseEntity(responseDto, HttpStatus.OK)
    }
}
