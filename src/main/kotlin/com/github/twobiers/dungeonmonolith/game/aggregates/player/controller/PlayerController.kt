package com.github.twobiers.dungeonmonolith.game.aggregates.player.controller

import com.github.twobiers.dungeonmonolith.game.aggregates.core.ErrorDetails
import com.github.twobiers.dungeonmonolith.game.aggregates.player.controller.dtos.CreatePlayerRequestDto
import com.github.twobiers.dungeonmonolith.game.aggregates.player.controller.dtos.PlayerResponseDto
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.Player
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.PlayerAlreadyExistsException
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.PlayerNotFoundException
import com.github.twobiers.dungeonmonolith.game.aggregates.player.repository.PlayerRepository
import com.github.twobiers.dungeonmonolith.game.aggregates.player.services.PlayerService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class PlayerController @Autowired constructor(
    private val playerService: PlayerService,
    private val playerRepository: PlayerRepository
){
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @ExceptionHandler(value = [PlayerNotFoundException::class])
    fun handlePlayerNotFound(e: PlayerNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorDetails(e.message, "Check if the provided player is registered."))
    }

    @ExceptionHandler(value = [PlayerAlreadyExistsException::class])
    fun handlePlayerAlreadyExists(e: PlayerAlreadyExistsException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ErrorDetails(e.message))
    }

    @PostMapping("/players", consumes = ["application/json"], produces = ["application/json"])
    fun createNewPlayer(@RequestBody requestPlayer: CreatePlayerRequestDto): ResponseEntity<PlayerResponseDto> {
        logger.trace("REST-Request to create new Player received ... [playerName={}]", requestPlayer.name)
        val newPlayer = playerService.createNewPlayer(requestPlayer.name, requestPlayer.email)
        val responsePlayer = PlayerResponseDto.makeFromPlayer(newPlayer)

        logger.debug("Request successful. Player created. [playerName={}, playerId={}]", newPlayer.getUserName(), newPlayer.getPlayerId())
        return ResponseEntity(responsePlayer, HttpStatus.CREATED)
    }

    @GetMapping("/players", produces = ["application/json"])
    fun getPlayer(@RequestParam(name = "name") userName: String, @RequestParam(name = "mail") userMail: String): ResponseEntity<PlayerResponseDto> {
        logger.trace("REST-Request to fetch Player-Details received ... [playerName={}]", userName)

        val player: Player = playerRepository.findByUserNameAndMailAddress(userName, userMail)
            .orElseThrow { PlayerNotFoundException("Player not found. [playerName=$userName, playerMail=$userMail]") }
        val responsePlayer = PlayerResponseDto.makeFromPlayer(player)

        logger.debug("Request successful. Player found. [playerName={}]", player.getUserName())
        return ResponseEntity(responsePlayer, HttpStatus.OK)
    }
}
