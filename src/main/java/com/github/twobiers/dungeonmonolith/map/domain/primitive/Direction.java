package com.github.twobiers.dungeonmonolith.map.domain.primitive;

public enum Direction {
  NORTH, EAST, SOUTH, WEST
}
