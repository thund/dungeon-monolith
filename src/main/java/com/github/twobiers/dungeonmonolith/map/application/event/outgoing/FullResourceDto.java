package com.github.twobiers.dungeonmonolith.map.application.event.outgoing;

import com.github.twobiers.dungeonmonolith.map.domain.planet.ResourceType;

public record FullResourceDto(
    ResourceType resourceType,
    int maxAmount,
    int currentAmount
) {

}
