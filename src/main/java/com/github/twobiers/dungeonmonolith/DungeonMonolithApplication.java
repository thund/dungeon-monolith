package com.github.twobiers.dungeonmonolith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DungeonMonolithApplication {

	public static void main(String[] args) {
		SpringApplication.run(DungeonMonolithApplication.class, args);
	}

}
