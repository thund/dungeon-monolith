package com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotDto(
    UUID id,
    UUID player
) { }
