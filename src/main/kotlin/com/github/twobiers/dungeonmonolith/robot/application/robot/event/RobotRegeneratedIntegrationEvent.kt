package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import java.util.UUID

data class RobotRegeneratedIntegrationEvent(
  val robotId: UUID,
  val availableEnergy: Int
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot) =
      RobotRegeneratedIntegrationEvent(
        robotId = robot.id,
        availableEnergy = robot.energy
      )
  }

  override fun key(): String = robotId.toString()
}
