package com.github.twobiers.dungeonmonolith.game.aggregates.game.events

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.game.aggregates.core.Event
import com.github.twobiers.dungeonmonolith.game.aggregates.core.EventBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class PlayerStatusEventBuilder @Autowired constructor(
) : EventBuilder {

    private val topic: String = "playerStatus"
    private val eventType: String = "player-status"
    private val version: Int = 1

    companion object {
        val objectMapper: ObjectMapper = ObjectMapper().findAndRegisterModules()
    }

    override fun deserializedEvent(serialized: String): Event {
        return objectMapper.readValue(serialized, PlayerStatusEvent::class.java)
    }

    fun makePlayerStatusEvent(transactionId: UUID, playerId: UUID, gameId: UUID, playerUsername: String): PlayerStatusEvent {
        return PlayerStatusEvent(
            id = UUID.randomUUID(),
            transactionId = transactionId,
            occurredAt = Instant.now(),
            eventName = eventType,
            topic = topic,
            version = version,
            playerId = playerId,
            playerUsername = playerUsername,
            gameId = gameId
        )
    }
}
