package com.github.twobiers.dungeonmonolith.map.domain.gameworld;

import com.github.twobiers.dungeonmonolith.map.domain.gameworld.events.GameworldCreated;
import com.github.twobiers.dungeonmonolith.map.domain.gameworld.events.GameworldStatusChanged;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Gameworld extends AbstractAggregateRoot<Gameworld> {
  @Id
  private UUID id = UUID.randomUUID();

  @Enumerated(EnumType.STRING)
  private GameworldStatus status = null;

  @Embedded
  private MapGrid mapGrid;

  public static Gameworld create(MapGrid mapGrid) {
    var gw = new Gameworld(mapGrid);
    gw.registerEvent(new GameworldCreated(gw));
    return gw;
  }

  private Gameworld(MapGrid mapGrid) {
    this.mapGrid = mapGrid;
  }

  public void changeStatus(GameworldStatus status) {
    this.status = status;
    this.registerEvent(new GameworldStatusChanged(this.id, this.status));
  }
}
