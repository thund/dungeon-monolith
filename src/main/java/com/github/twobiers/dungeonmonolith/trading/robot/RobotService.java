package com.github.twobiers.dungeonmonolith.trading.robot;

import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotInventoryUpdatedDto;
import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotSpawnedDto;
import java.util.UUID;

public interface RobotService {
    void create(RobotSpawnedDto robotSpawned);
    void delete(UUID robotId);
    TradingRobot findById(UUID robotId);
    void updateInventory(RobotInventoryUpdatedDto robotInventoryUpdated);
    void save(TradingRobot robot);
}
