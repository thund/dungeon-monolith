package com.github.twobiers.dungeonmonolith.robot.domain.planet

import com.fasterxml.jackson.annotation.JsonProperty

enum class MapDirection {
  @JsonProperty("north")
  NORTH,
  @JsonProperty("south")
  SOUTH,
  @JsonProperty("east")
  EAST,
  @JsonProperty("west")
  WEST
}
