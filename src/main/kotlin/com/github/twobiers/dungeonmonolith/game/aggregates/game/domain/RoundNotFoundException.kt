package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class RoundNotFoundException(override val message: String) : Exception(message) {
}
