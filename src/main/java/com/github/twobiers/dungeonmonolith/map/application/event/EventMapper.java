package com.github.twobiers.dungeonmonolith.map.application.event;

import com.github.twobiers.dungeonmonolith.map.application.event.outgoing.FullPlanetDto;
import com.github.twobiers.dungeonmonolith.map.application.event.outgoing.FullResourceDto;
import com.github.twobiers.dungeonmonolith.map.application.event.outgoing.GameworldCreatedDto;
import com.github.twobiers.dungeonmonolith.map.domain.gameworld.Gameworld;
import com.github.twobiers.dungeonmonolith.map.domain.gameworld.MapGrid;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class EventMapper {

  public GameworldCreatedDto map(Gameworld gameworld) {
    var planets = fromMapGrid(gameworld.getMapGrid());
    return new GameworldCreatedDto(gameworld.getId(), gameworld.getStatus(), planets);
  }

  private List<FullPlanetDto> fromMapGrid(MapGrid grid) {
    var planets = grid.getPlanets();
    return planets.entrySet().stream()
        .map(entry -> {
          var coords = entry.getKey();
          var planet = entry.getValue();
          var resource = planet.getResource();
          return new FullPlanetDto(
              planet.getId(),
              coords.getX(),
              coords.getY(),
              grid.areaOf(coords).getMovementDifficulty(),
              resource != null ? new FullResourceDto(
                  resource.getType(),
                  resource.getMaxAmount(),
                  resource.getCurrentAmount()
              ) : null
          );
        }).toList();
  }
}
