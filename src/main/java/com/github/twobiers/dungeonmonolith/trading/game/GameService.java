package com.github.twobiers.dungeonmonolith.trading.game;

import com.github.twobiers.dungeonmonolith.trading.game.dto.RoundDto;
import java.util.UUID;

public interface GameService
{
    void create(UUID id);
    TradingGame findById(UUID id);
    void start(UUID gameId);
    void stop(UUID gameId);
    void updateRound(RoundDto roundDto);
    void save(TradingGame game);
    void registerPlayer(UUID gameId, UUID playerId);
}

