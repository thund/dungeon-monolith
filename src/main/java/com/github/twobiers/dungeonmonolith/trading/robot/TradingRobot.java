package com.github.twobiers.dungeonmonolith.trading.robot;

import com.github.twobiers.dungeonmonolith.trading.core.LoggingEntityListener;
import com.github.twobiers.dungeonmonolith.trading.player.TradingPlayer;
import com.github.twobiers.dungeonmonolith.trading.robot.events.RobotCreated;
import com.github.twobiers.dungeonmonolith.trading.robot.events.RobotInventoryCleared;
import com.github.twobiers.dungeonmonolith.trading.robot.events.RobotInventoryUpdated;
import jakarta.persistence.Table;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
@EntityListeners(LoggingEntityListener.class)
@Table(name = "trading_robot")
public class TradingRobot extends AbstractAggregateRoot<TradingRobot> {
    @Id
    private UUID id;

    @ManyToOne(optional = false)
    private TradingPlayer player;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, Integer> inventory = new HashMap<>();

    public TradingRobot(UUID id, TradingPlayer player) {
        this.id = id;
        this.player = player;
        this.registerEvent(new RobotCreated(id, player.getId()));
    }

    public Map<String, Integer> getInventory() {
        return Map.copyOf(inventory);
    }

    public void clearInventory() {
        inventory.clear();
        this.registerEvent(new RobotInventoryCleared(id));
    }

    public void setInventory(Map<String, Integer> inventory) {
        this.inventory = inventory;
        this.registerEvent(new RobotInventoryUpdated(id, inventory));
    }
}
