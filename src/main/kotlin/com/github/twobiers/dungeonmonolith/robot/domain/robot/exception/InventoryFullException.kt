package com.github.twobiers.dungeonmonolith.robot.domain.robot.exception

class InventoryFullException(s: String) : RuntimeException(s)
