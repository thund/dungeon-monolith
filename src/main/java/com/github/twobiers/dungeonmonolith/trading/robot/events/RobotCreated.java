package com.github.twobiers.dungeonmonolith.trading.robot.events;

import java.util.UUID;

public record RobotCreated(
    UUID robotId,
    UUID playerId
) {

}
