package com.github.twobiers.dungeonmonolith.trading.robot;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface TradingRobotRepository extends CrudRepository<TradingRobot, UUID> {
}
