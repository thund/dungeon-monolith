package com.github.twobiers.dungeonmonolith.robot.infrastructure.trading.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.UUID

@JsonIgnoreProperties(ignoreUnknown = true)
data class TradableSoldDto(
  val playerId: UUID,
  val robotId: UUID,
  val type: TradableType,
  val name: String,
  val amount: Int
)
