package com.github.twobiers.dungeonmonolith.map.application.commands.dto;

public record CreateGameworldDto(int playerAmount) {}
