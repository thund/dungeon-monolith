package com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto

import java.util.*

class CommandResponseDto(
    val transactionId: UUID
)
