package com.github.twobiers.dungeonmonolith.map.application.event;

import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KafkaEventPublisher implements EventPublisher {
  private final KafkaTemplate<String, String> kafkaTemplate;

  @Override
  public <T> void publish(Message<T> event) {
    kafkaTemplate.send(event);
  }
}
