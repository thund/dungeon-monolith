package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class GameAlreadyActiveException(override val message: String): Exception(message) {
}
