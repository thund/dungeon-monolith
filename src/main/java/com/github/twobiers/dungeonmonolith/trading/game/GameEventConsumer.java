package com.github.twobiers.dungeonmonolith.trading.game;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.twobiers.dungeonmonolith.trading.game.dto.GameStatusDto;
import com.github.twobiers.dungeonmonolith.trading.game.dto.RoundDto;
import com.github.twobiers.dungeonmonolith.trading.player.events.PlayerRegistered;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GameEventConsumer {

    private final GameService gameService;

    private final ObjectMapper objectMapper;

    public GameEventConsumer(GameService gameService,
        ObjectMapper objectMapper) {
        this.gameService = gameService;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "status", groupId = "trading", autoStartup = "true")
    public void listenToGameStatus(@Payload GameStatusDto statusDto) {
        if (statusDto.status() == GameStatus.CREATED) {
            this.gameService.create(statusDto.gameId());
        } else if (statusDto.status() == GameStatus.ENDED) {
            this.gameService.stop(statusDto.gameId());
        } else if (statusDto.status() == GameStatus.STARTED) {
            this.gameService.start(statusDto.gameId());
        }
    }

    @KafkaListener(topics = "roundStatus", groupId = "trading", autoStartup = "true")
    public void listenToRoundStarted(@Payload RoundDto round) {
        this.gameService.updateRound(round);
    }

    @EventListener(PlayerRegistered.class)
    public void listenToPlayerStatus(PlayerRegistered playerRegistered) {
        this.gameService.registerPlayer(playerRegistered.gameId(), playerRegistered.id());
    }
}
