package com.github.twobiers.dungeonmonolith.trading.bank.exception;

public class BalanceNotSufficientException extends RuntimeException {
    public BalanceNotSufficientException(String message) {
        super(message);
    }
}
