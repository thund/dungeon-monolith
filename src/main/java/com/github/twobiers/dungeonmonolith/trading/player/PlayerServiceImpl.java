package com.github.twobiers.dungeonmonolith.trading.player;

import com.github.twobiers.dungeonmonolith.trading.player.dto.PlayerStatusDto;
import java.util.UUID;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class PlayerServiceImpl implements PlayerService {

    private final TradingPlayerRepository playerRepository;

    public PlayerServiceImpl(TradingPlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Transactional
    public void createOrUpdate(PlayerStatusDto playerStatusDto) {
        var playerId = playerStatusDto.playerId();
        var player = this.playerRepository.findById(playerId);
        TradingPlayer newPlayer;
        if(player.isPresent()) {
            newPlayer = player.get();
            newPlayer.setName(playerStatusDto.name());
        } else {
            newPlayer = new TradingPlayer(playerId, playerStatusDto.name());
        }
        this.playerRepository.save(newPlayer);
    }

    public TradingPlayer findPlayerByID(UUID playerId) {
        return this.playerRepository.findById(playerId).orElseThrow();
    }
}
