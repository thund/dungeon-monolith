package com.github.twobiers.dungeonmonolith.game.aggregates.player.domain

class PlayerNotFoundException(override val message: String) : Exception(message) {
}
