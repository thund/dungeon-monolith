package com.github.twobiers.dungeonmonolith.global;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.listener.CommonLoggingErrorHandler;
import org.springframework.kafka.support.converter.ByteArrayJsonMessageConverter;

@Configuration
public class MessagingConfig {
  @Bean
  public ByteArrayJsonMessageConverter byteArrayJsonMessageConverter() {
    return new ByteArrayJsonMessageConverter();
  }

  @Bean
  public CommonLoggingErrorHandler errorHandler(){
    return new CommonLoggingErrorHandler();
  }
}
