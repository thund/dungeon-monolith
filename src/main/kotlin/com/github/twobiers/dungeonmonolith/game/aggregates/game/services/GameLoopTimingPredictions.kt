package com.github.twobiers.dungeonmonolith.game.aggregates.game.services

import java.time.Instant

data class GameLoopTimingPredictions(
    val roundStart: Instant,
    val commandInputEnd: Instant,
    val roundEnd: Instant
)
