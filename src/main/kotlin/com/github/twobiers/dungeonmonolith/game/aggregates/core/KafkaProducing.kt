package com.github.twobiers.dungeonmonolith.game.aggregates.core

import com.github.twobiers.dungeonmonolith.game.aggregates.core.Event
import org.apache.kafka.clients.producer.ProducerRecord

interface KafkaProducing {

    fun send(event: Event)

    fun send(record: ProducerRecord<String, String>)
}
