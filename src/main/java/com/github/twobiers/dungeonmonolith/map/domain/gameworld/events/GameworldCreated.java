package com.github.twobiers.dungeonmonolith.map.domain.gameworld.events;

import com.github.twobiers.dungeonmonolith.map.domain.gameworld.Gameworld;

public record GameworldCreated(
    Gameworld gameworld
) {

}
