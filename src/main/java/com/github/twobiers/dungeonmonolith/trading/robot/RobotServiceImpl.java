package com.github.twobiers.dungeonmonolith.trading.robot;

import com.github.twobiers.dungeonmonolith.trading.player.PlayerService;
import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotInventoryUpdatedDto;
import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotSpawnedDto;
import java.util.Map;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class RobotServiceImpl implements RobotService {
    private final TradingRobotRepository tradingRobotRepository;
    private final PlayerService playerService;

    public RobotServiceImpl(TradingRobotRepository tradingRobotRepository, PlayerService playerService) {
        this.tradingRobotRepository = tradingRobotRepository;
        this.playerService = playerService;
    }

    public void create(RobotSpawnedDto robotSpawned) {
        var robotDto = robotSpawned.robot();
        var player = playerService.findPlayerByID(robotDto.player());
        var robot = new TradingRobot(robotDto.id(), player);
        this.tradingRobotRepository.save(robot);
    }

    @Override
    public void delete(UUID robotId) {
        this.tradingRobotRepository.deleteById(robotId);
    }

    @Override
    public void updateInventory(RobotInventoryUpdatedDto robotInventoryUpdated) {
        var robot = this.tradingRobotRepository.findById(robotInventoryUpdated.robot()).orElseThrow();
        var resources = robotInventoryUpdated.inventory().resources();
        var inventory = Map.of(
            "COAL", resources.coal(),
            "IRON", resources.iron(),
            "GEM", resources.gem(),
            "GOLD", resources.gold(),
            "PLATIN", resources.platin()
        );
        robot.setInventory(inventory);
        this.tradingRobotRepository.save(robot);
    }

    @Override
    public TradingRobot findById(UUID robotId) {
        return tradingRobotRepository.findById(robotId).orElseThrow();
    }

    @Override
    public void save(TradingRobot robot) {
        this.tradingRobotRepository.save(robot);
    }
}
