package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.command.RobotCommand
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotRegeneratedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import jakarta.transaction.Transactional

@Service
class RegenerateCommandHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {


  fun handle(command: RobotCommand) {
    val robot = robotRepository.findByIdOrThrow(command.robotId)
    robot.regenerate()
    this.robotRepository.save(robot)

    val event = RobotRegeneratedIntegrationEvent.build(robot)
    this.integrationEventPublisher.publish(event, command.transactionId)
  }
}
