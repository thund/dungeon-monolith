package com.github.twobiers.dungeonmonolith.trading.bank.event;

import java.util.UUID;

public interface BankAccountEvent {
    default String type() {
        return this.getClass().getSimpleName();
    }

    UUID playerId();

    String key();
    default String correlationId() {
        return key();
    }
}
