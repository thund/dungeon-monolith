package com.github.twobiers.dungeonmonolith.trading.bank.event;

import java.math.BigDecimal;
import java.util.UUID;

public record BankAccountCleared(
    UUID playerId,
    BigDecimal balance
) implements BankAccountEvent {

    @Override
    public String key() {
        return playerId.toString();
    }
}
