package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.application.event.ErrorEvent
import java.util.*

data class RobotCommandFailed(
  override val playerId: UUID,
  override val transactionId: UUID,
  override val robotId: UUID,
  override val description: String
) : ErrorEvent(playerId, transactionId, robotId, "ERR_ROBOT_COMMAND_FAILED", description)
