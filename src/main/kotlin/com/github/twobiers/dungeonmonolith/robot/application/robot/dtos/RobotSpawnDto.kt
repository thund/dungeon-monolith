package com.github.twobiers.dungeonmonolith.robot.application.robot.dtos

import java.util.*

data class RobotSpawnDto(
  val transactionId: UUID,
  val player: UUID,
  val quantity: Int
)
