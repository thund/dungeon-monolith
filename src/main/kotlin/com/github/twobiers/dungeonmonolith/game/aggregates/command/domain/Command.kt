package com.github.twobiers.dungeonmonolith.game.aggregates.command.domain

import com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto.CommandRequestDto
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.Round
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.Player
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain.GameRobot
import mu.KotlinLogging
import java.util.*
import jakarta.persistence.*

@Entity
@Table(
    name = "commands",
    indexes = [
        Index(name = "command_index_roundIdAndCommandType", columnList = "round_id, command_type"),
        Index(name = "command_index_roundIdAndPlayerId", columnList = "round_id, player_id")
    ],
    uniqueConstraints = [
        UniqueConstraint(
            name = "command_unique_roundIdAndPlayerIdAndRobotId",
            columnNames = ["round_id", "player_id", "robot_id"]
        )
    ]
)
class Command constructor(
  @Id
    @Column(name = "command_id")
    private val commandId: UUID = UUID.randomUUID(),

  @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "round_id")
    private val round: Round,

  @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    private val player: Player,

  @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "robot_id")
    private val robot: GameRobot?,

  @Column(name = "command_type")
    @Enumerated(EnumType.STRING)
    private var commandType: CommandType,

  @Embedded
    private var commandPayload: CommandPayload?
) {
    fun getCommandId(): UUID = commandId

    fun getPlayer(): Player = player

    fun getRobot(): GameRobot? = robot

    fun getCommandType(): CommandType = commandType

    fun getCommandPayload(): CommandPayload? = commandPayload

    override fun toString(): String {
        return "Command(commandId=$commandId, roundId=${round.getRoundId()}, roundNumber=${round.getRoundNumber()}, player=${player.getUserName()}, robot=${robot?.getRobotId()}, commandType='$commandType', commandPayload=$commandPayload)"
    }

    fun update(commandType: CommandType, commandPayload: CommandPayload?) {
        this.commandType = commandType
        this.commandPayload = commandPayload
    }

    companion object {
        private val logger = KotlinLogging.logger {}

        fun <A> parseCommandsToDto(commands: List<Command>, mapper: (Command) -> A): List<A> {
            val mappedCommands: MutableList<A> = mutableListOf()
            commands.forEach {
                try {
                    mappedCommands.add(mapper(it))
                } catch (ignored: Exception) { }
            }
            return mappedCommands.toList()
        }

        @Throws(CommandArgumentException::class)
        fun makeCommandFromDto(round: Round, player: Player, robot: GameRobot?, commandType: CommandType, dto: CommandRequestDto): Command {
            if (commandType != CommandType.BUYING && commandType != CommandType.SELLING  && robot == null) {
                logger.debug("Command-Creation failed. Robot not found but is required for this type of command. [commandType={}]", commandType)
                logger.trace{ dto.toString() }
                throw CommandArgumentException("Command-Creation failed. Robot not found but is required for this type of command. [commandType=$commandType]")
            }
            return Command(
                commandId = UUID.randomUUID(),
                round = round,
                player = player,
                robot = robot,
                commandType = commandType,
                commandPayload = CommandPayload(
                    planetId = dto.commandObject.planetId,
                    targetId = dto.commandObject.targetId,
                    itemName = dto.commandObject.itemName,
                    itemQuantity = dto.commandObject.itemQuantity
                )
            )
        }
    }
}
