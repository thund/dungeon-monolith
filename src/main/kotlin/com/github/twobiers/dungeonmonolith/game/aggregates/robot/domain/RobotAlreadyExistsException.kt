package com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain

class RobotAlreadyExistsException(message: String?) : Exception(message) {
}
