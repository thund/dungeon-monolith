package com.github.twobiers.dungeonmonolith.game.aggregates.game.controller.dto

import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

class JoinGameResponseDto(
    val gameExchange: String,
    val playerQueue: String
) {
}
