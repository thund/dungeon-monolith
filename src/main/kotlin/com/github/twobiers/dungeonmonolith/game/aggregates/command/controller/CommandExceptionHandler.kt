package com.github.twobiers.dungeonmonolith.game.aggregates.command.controller

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandArgumentException
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandParsingException
import com.github.twobiers.dungeonmonolith.game.aggregates.core.ErrorDetails
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.GameNotFoundException
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.GameParticipationException
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.GameStateException
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.PlayerNotFoundException
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.RoundNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

//@ControllerAdvice(basePackageClasses = [CommandExceptionHandler::class])
class CommandExceptionHandler {
    @ExceptionHandler
    fun handlePlayerNotFound(e: PlayerNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorDetails(e.message, "Check if the provided player is registered."))
    }

    @ExceptionHandler
    fun handleGameNotFound(e: GameNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorDetails(e.message, "Check if the provided game is created."))
    }

    @ExceptionHandler
    fun handleRoundNotFound(e: RoundNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorDetails(e.message, "Check if the game is running and the round began."))
    }

    @ExceptionHandler
    fun handleException(e: CommandArgumentException): ResponseEntity<ErrorDetails> {
        // Parsing exceptions are easier to debug with stack trace, so we don't hide it
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ErrorDetails(e.message, "Check your command.", e.stackTraceToString()))
    }

    @ExceptionHandler
    fun handleException(e: CommandParsingException): ResponseEntity<ErrorDetails> {
        // Parsing exceptions are easier to debug with stack trace, so we don't hide it
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(ErrorDetails(e.message, "Check your command.", e.stackTraceToString()))
    }

    @ExceptionHandler
    fun handleException(e: GameStateException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ErrorDetails(e.message, "Verify the game is in a state to accept commands."))
    }

    @ExceptionHandler
    fun handleException(e: GameParticipationException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(ErrorDetails(e.message, "Check if the player is participating in the game."))
    }
}
