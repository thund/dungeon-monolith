package com.github.twobiers.dungeonmonolith.robot.domain.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.core.DomainEvent
import java.util.*

sealed interface RobotDomainEvent : DomainEvent {
  fun robotId(): UUID

  /**
   * If the event is of interest for a (or maybe or players) the Player IDs will be set here.
   * As a robot event is ALWAYS of interest for the owning player, it is mandatory
   */
  fun playerIds(): List<UUID>
}
