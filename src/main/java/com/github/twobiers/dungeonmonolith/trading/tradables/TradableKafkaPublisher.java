package com.github.twobiers.dungeonmonolith.trading.tradables;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.twobiers.dungeonmonolith.trading.game.event.GameRoundStarted;
import com.github.twobiers.dungeonmonolith.trading.tradables.event.TradableBought;
import com.github.twobiers.dungeonmonolith.trading.tradables.event.TradableSold;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

@Service
public class TradableKafkaPublisher {
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;
    private final TradableRepository tradableRepository;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;

    public TradableKafkaPublisher(KafkaTemplate<String, String> kafkaTemplate,
        ObjectMapper objectMapper, TradableRepository tradableRepository) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
        this.tradableRepository = tradableRepository;
    }

    @TransactionalEventListener(TradableBought.class)
    public void onBuy(TradableBought event) throws JsonProcessingException {
        var message = this.objectMapper.writeValueAsString(event);
        var key = "{\"playerId\":\"" + event.playerId() + "\",\"robotId\":\"" + event.robotId() + "\"}";
        var producerRecord = new ProducerRecord<>("trade-buy", key, message);
        producerRecord.headers().add("playerId", event.playerId().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("eventId", UUID.randomUUID().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("type", "TradableBought".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("version", "1".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("transactionId", event.playerId().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("timestamp", dateTimeFormatter.format(Instant.now()).getBytes(
            StandardCharsets.UTF_8));
        this.kafkaTemplate.send(producerRecord);
    }

    @TransactionalEventListener(TradableSold.class)
    public void onSell(TradableSold event) throws JsonProcessingException {
        var message = this.objectMapper.writeValueAsString(event);
        var key = "{\"playerId\":\"" + event.playerId() + "\",\"robotId\":\"" + event.robotId() + "\"}";
        var producerRecord = new ProducerRecord<>("trade-sell", key, message);
        producerRecord.headers().add("playerId", event.playerId().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("eventId", UUID.randomUUID().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("type", "TradableSold".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("version", "1".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("transactionId", event.playerId().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("timestamp", dateTimeFormatter.format(Instant.now()).getBytes(
            StandardCharsets.UTF_8));
        this.kafkaTemplate.send(producerRecord);
    }

    @EventListener(GameRoundStarted.class)
    public void onRoundStart(GameRoundStarted event) throws JsonProcessingException {
        var tradables = this.tradableRepository.findAll();
        var message = this.objectMapper.writeValueAsString(tradables);

        var key = event.id().toString();
        var producerRecord = new ProducerRecord<>("prices", key, message);

        producerRecord.headers().add("eventId", UUID.randomUUID().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("type", "TradablePrices".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("version", "1".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("transactionId", event.id().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("timestamp", dateTimeFormatter.format(Instant.now()).getBytes(
            StandardCharsets.UTF_8));

        this.kafkaTemplate.send(producerRecord);
    }
}
