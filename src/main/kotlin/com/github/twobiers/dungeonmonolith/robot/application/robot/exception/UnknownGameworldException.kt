package com.github.twobiers.dungeonmonolith.robot.application.robot.exception

import com.github.twobiers.dungeonmonolith.robot.application.core.FailureException
import java.util.*

class UnknownGameworldException(gameworld: UUID) :
  FailureException("Map Service doesn't have a gameworld with the id $gameworld")
