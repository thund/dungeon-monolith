package com.github.twobiers.dungeonmonolith.trading.game.dto;

import java.util.UUID;

public record PlayerStatusDto(
    UUID gameId,
    UUID playerId
) {}
