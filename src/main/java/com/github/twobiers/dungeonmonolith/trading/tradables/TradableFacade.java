package com.github.twobiers.dungeonmonolith.trading.tradables;

import java.util.UUID;

public interface TradableFacade {

    void buyForPlayer(UUID playerId, String name, Integer amount);
    void buyForRobot(UUID robotId, String name, Integer amount);

    void sellInventory(UUID robotId);

}
