package com.github.twobiers.dungeonmonolith.trading.game;

import com.github.twobiers.dungeonmonolith.trading.game.dto.RoundDto;
import java.util.Optional;
import java.util.UUID;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {

    private final TradingGameRepository gameRepository;


    public GameServiceImpl(TradingGameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public void create(UUID id) {
        var game = new TradingGame(id);
        this.save(game);
    }

    @Override
    public TradingGame findById(UUID id) {
        return this.gameRepository.findById(id).orElseThrow();
    }

    @Override
    public void start(UUID gameId) {
        this.gameRepository.findByIsCurrentGame(true)
            .ifPresent(game -> stop(game.getGameId()));
        var game = this.findById(gameId);
        game.startGame();
        this.save(game);
    }

    @Override
    public void stop(UUID gameId) {
        var game = this.findById(gameId);
        game.stopGame();
        this.save(game);
    }

    @Transactional
    public void updateRound(RoundDto roundDto) {
        Optional<TradingGame> newGame = this.gameRepository.findById(roundDto.gameId());
        var game = newGame.orElseThrow();
        game.setRound(roundDto.roundNumber());
        this.save(game);
    }

    @Override
    public void save(TradingGame game) {
        this.gameRepository.save(game);
    }

    @Override
    public void registerPlayer(UUID gameId, UUID playerId) {
        var game = this.gameRepository.findById(gameId).orElseThrow();
        game.registerPlayer(playerId);
        this.gameRepository.save(game);
    }
}

