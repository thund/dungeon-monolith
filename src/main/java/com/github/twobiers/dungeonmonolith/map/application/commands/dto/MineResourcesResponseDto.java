package com.github.twobiers.dungeonmonolith.map.application.commands.dto;

public record MineResourcesResponseDto(int amountMined) {

}
