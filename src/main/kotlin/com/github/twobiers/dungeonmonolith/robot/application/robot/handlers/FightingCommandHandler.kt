package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.command.RobotCommand
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotAttackedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import jakarta.transaction.Transactional

@Service
class FightingCommandHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {


  fun handle(command: RobotCommand) {
    val targetId = command.targetId ?: throw RuntimeException("TargetId is null")

    val attacker = robotRepository.findByIdOrThrow(command.robotId)
    val target = robotRepository.findByIdOrThrow(targetId)

    attacker.attack(target)

    this.robotRepository.save(attacker)
    this.robotRepository.save(target)

    val event = RobotAttackedIntegrationEvent.build(attacker, target)
    this.integrationEventPublisher.publish(event, command.transactionId)
  }
}
