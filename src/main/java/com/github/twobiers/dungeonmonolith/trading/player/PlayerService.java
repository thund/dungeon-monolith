package com.github.twobiers.dungeonmonolith.trading.player;

import com.github.twobiers.dungeonmonolith.trading.player.dto.PlayerStatusDto;
import java.util.UUID;

public interface PlayerService {
    void createOrUpdate(PlayerStatusDto playerStatusDto);
    TradingPlayer findPlayerByID(UUID playerId);
}
