package com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandType
import java.util.UUID

data class RobotCommandDto(
    val robotId: UUID,
    val transactionId: UUID,
    val playerId: UUID,
    val command: CommandType,
    val planetId: UUID?,
    val targetId: UUID?
) {
    companion object {
        fun makeFromCommand(command: Command): RobotCommandDto {
            return RobotCommandDto(
                command.getRobot()!!.getRobotId(),
                command.getCommandId(),
                command.getPlayer().getPlayerId(),
                command.getCommandType(),
                command.getCommandPayload()?.getPlanetId(),
                command.getCommandPayload()?.getTargetId()
            )
        }
    }
}
