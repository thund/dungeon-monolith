package com.github.twobiers.dungeonmonolith.game.aggregates.game.repositories

import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.Game
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.GameStatus
import org.springframework.data.repository.CrudRepository
import java.util.*

interface GameRepository : CrudRepository<Game, UUID> {

    fun existsByGameStatusIn(gameStatus: List<GameStatus>): Boolean
    fun findAllByGameStatusIn(gameStatus: List<GameStatus>): List<Game>
}
