package com.github.twobiers.dungeonmonolith.trading.bank;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountEvent;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class BankAccountKafkaPublisher {
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;

    public BankAccountKafkaPublisher(KafkaTemplate<String, String> kafkaTemplate,
        ObjectMapper objectMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
    }

    @EventListener(BankAccountEvent.class)
    public void onBankAccountEvent(BankAccountEvent event) throws JsonProcessingException {
        var message = this.objectMapper.writeValueAsString(event);
        var producerRecord = new ProducerRecord<>("bank", event.key(), message);
        producerRecord.headers().add("playerId", event.playerId().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("eventId", UUID.randomUUID().toString().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("type", event.type().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("version", "1".getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("transactionId", event.correlationId().getBytes(
            StandardCharsets.UTF_8));
        producerRecord.headers().add("timestamp", dateTimeFormatter.format(Instant.now()).getBytes(
            StandardCharsets.UTF_8));
        this.kafkaTemplate.send(producerRecord);
    }
}
