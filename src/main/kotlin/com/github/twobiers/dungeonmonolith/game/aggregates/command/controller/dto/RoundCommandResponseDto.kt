package com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandType
import java.util.*

class RoundCommandResponseDto (
    val transactionId: UUID,
    val gameId: UUID,
    val playerId: UUID,
    val robotId: UUID?,
    val commandType: CommandType,
    val commandObject: CommandObjectRequestDto
) {
    constructor(gameId: UUID, command: Command): this (
        transactionId = command.getCommandId(),
        gameId = gameId,
        playerId = command.getPlayer().getPlayerId(),
        robotId = command.getRobot()?.getRobotId(),
        commandType = command.getCommandType(),
        CommandObjectRequestDto(command)
    )

    override fun equals(other: Any?): Boolean =
        (other is RoundCommandResponseDto)
                && transactionId == other.transactionId
                && gameId == other.gameId
                && playerId == other.playerId
                && robotId == other.robotId
                && commandType == other.commandType
                && commandObject == other.commandObject
}
