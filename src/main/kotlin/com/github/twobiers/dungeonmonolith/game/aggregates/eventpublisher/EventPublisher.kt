package com.github.twobiers.dungeonmonolith.game.aggregates.eventpublisher

import com.github.twobiers.dungeonmonolith.game.aggregates.core.Event
import com.github.twobiers.dungeonmonolith.game.aggregates.core.KafkaProducing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EventPublisher @Autowired constructor(
    private val kafkaProducing: KafkaProducing
) {
    fun publishEvents(events: List<Event>) {
        events.forEach { event: Event ->
            kafkaProducing.send(event)
        }
    }

    fun publishEvent(event: Event) {
        kafkaProducing.send(event)
    }
}
