package com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto

import com.fasterxml.jackson.annotation.JsonAlias

enum class RoundStatus {
  @JsonAlias("started")
  STARTED,
  @JsonAlias("command input ended")
  COMMAND_INPUT_ENDED,
  @JsonAlias("ended")
  ENDED
}
