package com.github.twobiers.dungeonmonolith.trading.bank.event;

import java.math.BigDecimal;
import java.util.UUID;

public record BankAccountDepositRequested(
    UUID playerId,
    BigDecimal depositAmount
) {

}
