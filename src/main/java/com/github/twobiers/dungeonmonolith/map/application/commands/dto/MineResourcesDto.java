package com.github.twobiers.dungeonmonolith.map.application.commands.dto;

public record MineResourcesDto(int amountToMine) {

}
