package com.github.twobiers.dungeonmonolith.map.application.event.outgoing;

import java.util.List;
import java.util.UUID;

public record PlanetDiscoveredDto(
    UUID planet,
    Integer movementDifficulty,
    List<PlanetNeighbourDto> neighbours,
    FullResourceDto resource
) {

}
