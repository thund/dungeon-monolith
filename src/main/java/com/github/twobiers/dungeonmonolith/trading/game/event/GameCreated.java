package com.github.twobiers.dungeonmonolith.trading.game.event;

import java.util.UUID;

public record GameCreated(
    UUID id
) {
}
