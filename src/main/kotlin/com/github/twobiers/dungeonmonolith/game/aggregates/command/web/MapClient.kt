package com.github.twobiers.dungeonmonolith.game.aggregates.command.web

import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto.NewGameWorldResponseDto
import com.github.twobiers.dungeonmonolith.map.application.commands.CreateNewGameworld
import com.github.twobiers.dungeonmonolith.map.application.commands.dto.CreateGameworldDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
@Scope("singleton")
class MapClient @Autowired constructor(
    val createGameworld: CreateNewGameworld
) {

    fun createNewGameWorld(numberOfPlayer: Int): NewGameWorldResponseDto {
        val response = createGameworld.create(CreateGameworldDto(numberOfPlayer))
        return NewGameWorldResponseDto(response.gameworldId)
    }
}
