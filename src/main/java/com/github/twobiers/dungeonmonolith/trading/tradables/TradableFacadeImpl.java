package com.github.twobiers.dungeonmonolith.trading.tradables;

import com.github.twobiers.dungeonmonolith.trading.robot.RobotService;
import jakarta.transaction.Transactional;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TradableFacadeImpl implements TradableFacade {

    private final RobotService robotService;
    private final TradableService tradableService;

    public TradableFacadeImpl(RobotService robotService, TradableService tradableService) {
        this.robotService = robotService;
        this.tradableService = tradableService;
    }

    @Override
    public void buyForPlayer(UUID playerId, String name, Integer amount) {
        log.debug("Buying {} {} for player {}", amount, name, playerId);
        this.tradableService.buy(playerId, null, name, amount);
    }

    @Override
    public void buyForRobot(UUID robotId, String name, Integer amount) {
        log.debug("Buying {} {} for robot {}", amount, name, robotId);
        var robot = robotService.findById(robotId);
        this.tradableService.buy(robot.getPlayer().getId(), robotId, name, amount);
    }

    @Override
    @Transactional
    public void sellInventory(UUID robotId) {
        log.debug("Selling inventory for robot {}", robotId);

        var robot = robotService.findById(robotId);

        var inventory = robot.getInventory();

        log.trace("Inventory: {}", inventory);

        inventory
            .entrySet()
            .stream().filter(e -> e.getValue() > 0)
            .forEach(
                (entry) -> tradableService.sell(robot.getPlayer().getId(), robotId,
                    entry.getKey().toUpperCase(),
                    entry.getValue()));
    }
}
