package com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.UUID

@JsonIgnoreProperties(ignoreUnknown = true)
data class GameStatusEvent (
    val gameId: UUID,
    val status: GameStatus
) {
}
