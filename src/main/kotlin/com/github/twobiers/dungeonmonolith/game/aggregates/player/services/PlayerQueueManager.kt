package com.github.twobiers.dungeonmonolith.game.aggregates.player.services

import com.rabbitmq.client.ShutdownSignalException
import org.springframework.amqp.AmqpIOException
import org.springframework.amqp.core.*
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

/**
 * This class is responsible for managing the player-related queues
 */
@Service
class PlayerQueueManager @Autowired constructor(
    private val rabbitAdmin: RabbitAdmin
) {
    private fun buildPlayerQueue(player: UUID): Queue {
        return QueueBuilder.durable(PLAYER_QUEUE_NAME(player))
            // .exclusive() TODO: Maybe we want to make this queue exclusive?
            .build()
    }

    private fun buildGameExchange(): Exchange = ExchangeBuilder
        .directExchange(GAME_QUEUE_NAME)
        .build()

    /**
     * Setups the player Queue for a single player.
     */
    fun setupPlayerQueue(player: UUID): Binding {
        val playerQueue = buildPlayerQueue(player)
        val gameExchange = buildGameExchange()

        rabbitAdmin.declareQueue(playerQueue)
        // Purge in case it already exists to prevent old messages from being consumed
        rabbitAdmin.purgeQueue(playerQueue.name, true)

        val playerBinding = BindingBuilder
            .bind(playerQueue)
            .to(gameExchange)
            .with(player.toString())
            .noargs()
        rabbitAdmin.declareBinding(playerBinding)

        val publicBinding = BindingBuilder
            .bind(playerQueue)
            .to(gameExchange)
            .with(PUBLIC_EVENT_BINDING_KEY)
            .noargs()

        rabbitAdmin.declareBinding(publicBinding)

        return playerBinding
    }

    /**
     * Creates and declares the global game exchange other which game-related events are
     * sent
     */
    fun setupGameExchange() {
        val gameExchange = buildGameExchange()
        rabbitAdmin.declareExchange(gameExchange)
    }

    companion object {
        const val PUBLIC_EVENT_BINDING_KEY = "public"

        // Note: At the moment we are using a single queue for every game.
        const val GAME_QUEUE_NAME = "game"
        val PLAYER_QUEUE_NAME: (UUID) -> String = { "player-${it}" }
    }
}
