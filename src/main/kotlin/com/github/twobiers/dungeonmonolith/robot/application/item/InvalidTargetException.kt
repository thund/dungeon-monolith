package com.github.twobiers.dungeonmonolith.robot.application.item

class InvalidTargetException(s: String) : RuntimeException(s)
