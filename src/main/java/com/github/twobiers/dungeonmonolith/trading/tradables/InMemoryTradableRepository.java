package com.github.twobiers.dungeonmonolith.trading.tradables;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Repository;

@Repository
public class InMemoryTradableRepository implements TradableRepository {

    private static final List<Tradable> tradables = List.of(
        new Tradable("ROBOT", TradableType.ITEM, BigDecimal.valueOf(100)),

        new Tradable("COAL", TradableType.RESOURCE, BigDecimal.valueOf(5)),
        new Tradable("IRON", TradableType.RESOURCE, BigDecimal.valueOf(15)),
        new Tradable("GEM", TradableType.RESOURCE, BigDecimal.valueOf(30)),
        new Tradable("GOLD", TradableType.RESOURCE, BigDecimal.valueOf(50)),
        new Tradable("PLATIN", TradableType.RESOURCE, BigDecimal.valueOf(60)),

        new Tradable("HEALTH_RESTORE", TradableType.RESTORATION, BigDecimal.valueOf(50)),
        new Tradable("ENERGY_RESTORE", TradableType.RESTORATION, BigDecimal.valueOf(75)),

        new Tradable("STORAGE_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("STORAGE_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("STORAGE_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("STORAGE_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("STORAGE_5", TradableType.UPGRADE, BigDecimal.valueOf(15000)),

        new Tradable("HEALTH_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("HEALTH_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("HEALTH_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("HEALTH_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("HEALTH_5", TradableType.UPGRADE, BigDecimal.valueOf(15000)),

        new Tradable("DAMAGE_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("DAMAGE_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("DAMAGE_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("DAMAGE_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("DAMAGE_5", TradableType.UPGRADE, BigDecimal.valueOf(15000)),

        new Tradable("MINING_SPEED_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("MINING_SPEED_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("MINING_SPEED_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("MINING_SPEED_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("MINING_SPEED_5", TradableType.UPGRADE, BigDecimal.valueOf(15000)),

        new Tradable("MINING_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("MINING_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("MINING_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("MINING_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("MINING_5", TradableType.UPGRADE, BigDecimal.valueOf(15000)),

        new Tradable("MAX_ENERGY_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("MAX_ENERGY_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("MAX_ENERGY_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("MAX_ENERGY_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("MAX_ENERGY_5", TradableType.UPGRADE, BigDecimal.valueOf(15000)),

        new Tradable("ENERGY_REGEN_1", TradableType.UPGRADE, BigDecimal.valueOf(50)),
        new Tradable("ENERGY_REGEN_2", TradableType.UPGRADE, BigDecimal.valueOf(300)),
        new Tradable("ENERGY_REGEN_3", TradableType.UPGRADE, BigDecimal.valueOf(1500)),
        new Tradable("ENERGY_REGEN_4", TradableType.UPGRADE, BigDecimal.valueOf(4000)),
        new Tradable("ENERGY_REGEN_5", TradableType.UPGRADE, BigDecimal.valueOf(15000))
    );

    private static final Map<String, Tradable> tradableMap = tradables.stream()
        .collect(Collectors.toMap(Tradable::getName, t -> t));

    @Override
    public Tradable findByName(String name) {
        return tradableMap.get(name.toUpperCase());
    }

    @Override
    public List<Tradable> findByType(TradableType type) {
        return tradableMap.values().stream()
            .filter(t -> t.getType().equals(type))
            .toList();
    }

    @Override
    public List<Tradable> findAll() {
        return tradableMap.values().stream().toList();
    }
}
