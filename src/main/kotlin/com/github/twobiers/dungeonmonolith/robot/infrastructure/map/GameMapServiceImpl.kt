package com.github.twobiers.dungeonmonolith.robot.infrastructure.map

import com.github.twobiers.dungeonmonolith.map.application.commands.MineResources
import com.github.twobiers.dungeonmonolith.map.application.commands.dto.MineResourcesDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class GameMapServiceImpl(
  @Autowired val mineResources: MineResources
) : GameMapService {

  companion object {
    val logger = KotlinLogging.logger {}
  }
  override fun mine(planetId: UUID, amount: Int): Int {
    return try {
      mineResources.mine(planetId, MineResourcesDto(amount)).amountMined
    } catch (e: Exception) {
      logger.warn(e) { "Failed to mine $amount resources from planet $planetId" }
      0
    }
  }
}
