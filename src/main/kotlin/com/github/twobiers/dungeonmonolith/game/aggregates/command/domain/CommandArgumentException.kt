package com.github.twobiers.dungeonmonolith.game.aggregates.command.domain

class CommandArgumentException(override val message: String) : Exception(message) {
}
