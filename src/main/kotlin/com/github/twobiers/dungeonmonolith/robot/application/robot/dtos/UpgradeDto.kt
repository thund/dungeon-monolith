package com.github.twobiers.dungeonmonolith.robot.application.robot.dtos

import com.github.twobiers.dungeonmonolith.robot.domain.robot.UpgradeType
import java.util.*

data class UpgradeDto(
  val transactionId: UUID,

  val upgradeType: UpgradeType,

  val targetLevel: Int
)
