package com.github.twobiers.dungeonmonolith.trading.game;

import com.github.twobiers.dungeonmonolith.trading.game.event.GameCreated;
import com.github.twobiers.dungeonmonolith.trading.game.event.GameRoundStarted;
import com.github.twobiers.dungeonmonolith.trading.game.event.GameStarted;
import com.github.twobiers.dungeonmonolith.trading.game.event.GameStopped;
import jakarta.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

/**
 * Game manages the round count and status for our service
 * needed for the calculation of item and resource prices
 * needed for the saving of item and resource histories
 */
@Entity
@Getter
@NoArgsConstructor
@Table(name = "trading_game")
public class TradingGame extends AbstractAggregateRoot<TradingGame> {
    @Id
    private UUID gameId;

    private Boolean isCurrentGame;

    private int currentRound;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<UUID> registeredPlayers = new ArrayList<>();


    public TradingGame(UUID newGame) {
        this.gameId = newGame;
        this.currentRound = 0;
        this.isCurrentGame = false;
        this.registerEvent(new GameCreated(newGame));
    }

    public void setRound(int round) {
        this.currentRound = round;
        this.registerEvent(new GameRoundStarted(this.gameId, this.currentRound));
    }

    public void stopGame() {
        this.isCurrentGame = false;
        this.registerEvent(new GameStopped(this.gameId));
    }

    public void startGame() {
        this.isCurrentGame = true;
        this.registerEvent(new GameStarted(this.gameId, this.registeredPlayers));
    }

    public void registerPlayer(UUID id) {
        this.registeredPlayers.add(id);
    }

    public List<UUID> getRegisteredPlayers() {
        return List.copyOf(this.registeredPlayers);
    }
}
