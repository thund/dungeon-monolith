package com.github.twobiers.dungeonmonolith.game.aggregates.player.services

import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.Player
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.PlayerAlreadyExistsException
import com.github.twobiers.dungeonmonolith.game.aggregates.player.repository.PlayerRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PlayerService @Autowired constructor(
    private val playerRepository: PlayerRepository
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @Throws(PlayerAlreadyExistsException::class)
    fun createNewPlayer(userName: String, mailAddress: String): Player {
        if(playerRepository.findByUserNameOrMailAddress(userName, mailAddress).isPresent) {
            logger.debug("Failed to create new Player. Player already exists. [name={}]", userName)
            throw PlayerAlreadyExistsException("Player with name $userName already exists.")
        }

        val player = Player(userName, mailAddress)
        playerRepository.save(player)
        logger.trace("New Player created. [name=${player.getUserName()}]")
        logger.trace { player.toString() }

        return player
    }
}
