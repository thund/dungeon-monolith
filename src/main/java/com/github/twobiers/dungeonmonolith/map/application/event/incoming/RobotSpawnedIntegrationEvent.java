package com.github.twobiers.dungeonmonolith.map.application.event.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotSpawnedIntegrationEvent(
  InnerRobot robot
) {
  public record InnerRobot(
      Planet planet
  ) {}

  public record Planet(
      UUID planetId
  ) {}
}
