package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotsRevealedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class RevealRobotsHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle() {
    val robots = robotRepository.findAllAlive()

    val event = RobotsRevealedIntegrationEvent.build(robots)
    this.integrationEventPublisher.publish(event)
  }
}
