package com.github.twobiers.dungeonmonolith.game.aggregates.game.services

import java.time.Instant

data class GameLoopTimings(
    val roundStarted: Instant,
    var commandInputEnded: Instant? = null,
    var roundEnded: Instant? = null
)
