package com.github.twobiers.dungeonmonolith.trading.tradables.event;

import com.github.twobiers.dungeonmonolith.trading.tradables.TradableType;
import java.math.BigDecimal;
import java.util.UUID;

public record TradableSold(
    UUID playerId,
    UUID robotId,
    TradableType type,
    String name,
    Integer amount,
    BigDecimal pricePerUnit,
    BigDecimal totalPrice
) {
}
