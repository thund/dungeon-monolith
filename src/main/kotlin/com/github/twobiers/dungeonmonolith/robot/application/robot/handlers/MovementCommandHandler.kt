package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.command.RobotCommand
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotMovedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.application.robot.exception.TargetPlanetNotReachableException
import com.github.twobiers.dungeonmonolith.robot.application.robot.exception.UnknownPlanetException
import com.github.twobiers.dungeonmonolith.robot.domain.planet.RobotPlanetRepository
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import jakarta.transaction.Transactional

@Service
class MovementCommandHandler(
  val robotRepository: RobotRepository,
  val robotPlanetRepository: RobotPlanetRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {

  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(command: RobotCommand) {
    val robotId = command.robotId
    val planetId = command.planetId ?: throw RuntimeException("PlanetId is null")

    val robot = robotRepository.findByIdOrThrow(robotId)
    val sourcePlanet = robot.planet

    val isNeighbour = sourcePlanet.isNeighbourTo(planetId)
    if (!isNeighbour) {
      throw TargetPlanetNotReachableException("Planet $planetId is not a neighbour of ${robot.planet.planetId}")
    }

    val targetPlanet =
      robotPlanetRepository.findById(planetId).orElseThrow { UnknownPlanetException(planetId) }

    robot.move(targetPlanet)
    this.robotRepository.save(robot)

    val event = RobotMovedIntegrationEvent.build(robot, sourcePlanet, targetPlanet)
    this.integrationEventPublisher.publish(event, command.transactionId)
  }
}
