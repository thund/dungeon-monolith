package com.github.twobiers.dungeonmonolith.robot.application.planet

import com.github.twobiers.dungeonmonolith.robot.domain.planet.PlanetType
import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import java.util.*

data class PlanetDto(
  val planetId: UUID,
  val movementDifficulty: Int,
  val planetType: PlanetType?,
  val resourceType: ResourceType?
)
