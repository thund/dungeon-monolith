package com.github.twobiers.dungeonmonolith.robot.domain.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import java.util.*

data class RobotSpawned(
  private val playerId: UUID,
  val robot: Robot
) : RobotDomainEvent {
  override fun robotId(): UUID = robot.id
  override fun playerIds(): List<UUID> = listOf(playerId)
}
