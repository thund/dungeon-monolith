package com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain

import mu.KotlinLogging
import java.util.*
import jakarta.persistence.*

@Entity
@Table(
    name = "game_robots"
)
class GameRobot constructor(
    @Id
    @Column(name="robot_id")
    
    private val robotId: UUID,

    
    @Column(nullable = false)
    private val playerId: UUID,

    @Column(name = "robot_status")
    private var robotStatus: RobotStatus = RobotStatus.ACTIVE
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    fun getRobotId(): UUID = robotId

    fun getPlayerId(): UUID = this.playerId

    fun getRobotStatus(): RobotStatus = robotStatus

    fun destroyRobot() {
        robotStatus = RobotStatus.INACTIVE
        logger.trace("RobotStatus set to INACTIVE.")
    }

    override fun toString(): String {
        return "Robot(robotId=$robotId, playerId=${this.playerId}, robotStatus=$robotStatus)"
    }
}
