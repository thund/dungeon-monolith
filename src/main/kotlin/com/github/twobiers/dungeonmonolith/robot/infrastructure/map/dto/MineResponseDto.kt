package com.github.twobiers.dungeonmonolith.robot.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class MineResponseDto(
  val amountMined: Int
)
