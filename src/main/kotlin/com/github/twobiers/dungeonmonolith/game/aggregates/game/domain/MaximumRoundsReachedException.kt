package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class MaximumRoundsReachedException : Exception() {
    override val message: String
        get() = "Maximum number of rounds reached"
}
