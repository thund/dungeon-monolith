package com.github.twobiers.dungeonmonolith.robot.application.robot

import com.github.twobiers.dungeonmonolith.robot.application.robot.dtos.*
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.SpawnRobotHandler
import com.github.twobiers.dungeonmonolith.robot.application.robot.mappers.RobotMapper
import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*

@Controller
@RequestMapping("robots")
class RobotController(
  val spawnRobotHandler: SpawnRobotHandler,
  val robotMapper: RobotMapper,
  val robotRepository: RobotRepository
) {

  /**
   * Spawns a new robot with the attributes given in [RobotSpawnDto] and returns a full [RobotDto] for the
   * newly created robot.
   *
   * @see <a href="https://the-microservice-dungeon.github.io/docs/openapi/robot#tag/robot/paths/~1robots/post"></a>
   */
  @PostMapping
  fun spawnRobots(@RequestBody spawnDto: RobotSpawnDto): ResponseEntity<List<RobotDto>> {
    val robots = mutableListOf<Robot>()
    for (i in 0 until spawnDto.quantity)
      robots.add(
        spawnRobotHandler.handle(
          spawnDto.player
        )
      )
    return ResponseEntity.status(HttpStatus.CREATED).body(
      robots.map { robot -> robotMapper.robotToRobotDto(robot) }
    )
  }

  /**
   * Get all robots of the specified player.
   *
   * @see <a href="https://the-microservice-dungeon.github.io/docs/openapi/robot#tag/robot/paths/~1robots/get"></a>
   * @return A List of [RobotDto]s
   */
  @GetMapping
  fun getRobotsOfPlayer(@RequestParam("player-id") playerId: UUID): ResponseEntity<List<RobotDto>> {
    val robots = robotRepository.findAllByPlayer(playerId)
    return ResponseEntity.ok(robotMapper.robotsToRobotDtos(robots))
  }

  /**
   * Get a specific robot by its UUID.
   *
   * @see <a href="https://the-microservice-dungeon.github.io/docs/openapi/robot#tag/robot/paths/~1robots~1{robot-uuid}/get"></a>
   * @return a [RobotDto] of the specified robot
   */
  @GetMapping("/{id}")
  fun getRobot(@PathVariable("id") robotId: UUID): ResponseEntity<RobotDto> {
    return robotRepository.findById(robotId)
      .map { ResponseEntity.ok(robotMapper.robotToRobotDto(it)) }
      .orElseGet { ResponseEntity.status(404).build() }
  }
}
