package com.github.twobiers.dungeonmonolith.robot.domain.robot.exception

class NotEnoughResourcesException(s: String) : RuntimeException(s)
