package com.github.twobiers.dungeonmonolith.game.aggregates.command.services

import com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto.CommandRequestDto
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandArgumentException
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandType
import com.github.twobiers.dungeonmonolith.game.aggregates.command.event.CommandCreated
import com.github.twobiers.dungeonmonolith.game.aggregates.command.event.CommandDispatched
import com.github.twobiers.dungeonmonolith.game.aggregates.command.repositories.CommandRepository
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.RobotClient
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.TradingClient
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto.BuyCommandDto
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto.SellCommandDto
import com.github.twobiers.dungeonmonolith.game.aggregates.eventpublisher.EventPublisher
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.*
import com.github.twobiers.dungeonmonolith.game.aggregates.game.repositories.GameRepository
import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.PlayerNotFoundException
import com.github.twobiers.dungeonmonolith.game.aggregates.player.repository.PlayerRepository
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.repositories.GameRobotRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataAccessException
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import org.springframework.stereotype.Service
import java.sql.SQLException
import java.util.*
import jakarta.persistence.PersistenceException
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.annotation.Isolation

@Service
class CommandService @Autowired constructor(
  private val commandRepository: CommandRepository,
  private val robotRepository: GameRobotRepository,
  private val gameRepository: GameRepository,
  private val playerRepository: PlayerRepository,
  private val robotClient: RobotClient,
  private val tradingClient: TradingClient,
  private val eventPublisher: EventPublisher
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @Transactional(rollbackFor = [Exception::class], isolation = Isolation.REPEATABLE_READ)
    @Throws(PlayerNotFoundException::class, GameNotFoundException::class, GameStateException::class, CommandArgumentException::class)
    // There's a probability for getting Lock exceptions on parallel requests. The issue is that the
    // Command Entity is referencing other entities directly and might be modified by concurrent
    // requests. Therefore, we can get a Lock exception when trying to save the Command Entity.
    // The most pragmatic way for now is to retry the request.
    // A better way would be to refactor the Command Entity to not reference other entities directly
    // but use scalar objects like the UUIDs instead. This would require a lot of refactoring though.
    @Retryable(value = [PersistenceException::class, DataAccessException::class], maxAttempts = 5, backoff = Backoff(delay = 50))
    fun createNewCommand(gameId: UUID, playerId: UUID, robotId: UUID?, commandType: CommandType, commandRequestDTO: CommandRequestDto): UUID {
        val player = playerRepository.findById(playerId)
            .orElseThrow {
                // Notice that we rely on the player ID to be confident as some kind of authentication
                // mechanism. However, we can safely log the ID for debugging, as obviously no
                // player with that ID exists.
                PlayerNotFoundException("Player with ID $playerId not found.")
            }
        val game: Game = gameRepository.findById(gameId)
            .orElseThrow { GameNotFoundException("Game with ID $gameId not found.") }
        val round: Round = game.getCurrentRound() ?: throw GameStateException("Game not in a state to accept commands. [gameStatus=${game.getGameStatus()}]")

        if (!game.isParticipating(player)) {
            throw GameParticipationException("Player '${player.getUserName()}' is not participating in the game.")
        }

        if (round.getRoundStatus() != RoundStatus.COMMAND_INPUT_STARTED) {
            throw GameStateException("Game not in a state to accept commands. [roundStatus=${round.getRoundStatus()}]")
        }

        // This is some 200IQ Optional and nullability handling.
        // I don't know how to write this in a more readable and secure way without bringing in new
        // abstractions. I think for the time being we can live with that.
        val robot = if(robotId == null) null else robotRepository.findById(robotId).orElse(null)

        var newCommand = Command.makeCommandFromDto(
            round = round,
            player = player,
            robot = robot,
            commandType = commandType,
            dto = commandRequestDTO
        )

        val existingCommand = commandRepository.findCommandByRoundAndPlayerAndRobot(round.getRoundId(), player.getPlayerId(), robot?.getRobotId())
        if (existingCommand != null) {
            existingCommand.update(commandType, newCommand.getCommandPayload())
            newCommand = existingCommand
        }

        val transactionId = newCommand.getCommandId()
        commandRepository.save(newCommand)
        logger.debug("New Command successfully created. [playerName={}, commandType={}, roundNumber={}]",
            player.getUserName(), commandType, round.getRoundNumber())

        eventPublisher.publishEvent(CommandCreated(newCommand))
        return transactionId
    }

    @Transactional(rollbackFor = [Exception::class])
    fun dispatchCommands(roundId: UUID, type: CommandType) {
        val commands: List<Command> = commandRepository.findAllCommandsByRoundAndCommandType(roundId, type)

        when (type) {
            CommandType.SELLING -> tradingClient.sendSellingCommands(commands.map { SellCommandDto.makeFromCommand(it) })
            CommandType.BUYING -> tradingClient.sendBuyingCommands(commands.map { BuyCommandDto.makeFromCommand(it) })
            CommandType.MINING, CommandType.MOVEMENT, CommandType.BATTLE, CommandType.REGENERATE -> robotClient.sendCommands(commands)
        }

        val dispatched = commands.map { CommandDispatched(it) }
        eventPublisher.publishEvents(dispatched)
    }
}
