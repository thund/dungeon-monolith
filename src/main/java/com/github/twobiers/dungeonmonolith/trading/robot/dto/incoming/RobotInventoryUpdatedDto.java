package com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming;

import java.util.UUID;

public record RobotInventoryUpdatedDto(
    UUID robot,
    RobotInventoryDto inventory
) {

    public record RobotInventoryDto(RobotResourcesDto resources) {}

}
