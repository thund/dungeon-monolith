package com.github.twobiers.dungeonmonolith.robot.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

@JsonIgnoreProperties(ignoreUnknown = true)
data class GameworldCreatedEventDto(
  val id: UUID,
  val status: String,
  val planets: List<GameworldCreatedEventPlanetDto>,
) {
  @JsonIgnoreProperties(ignoreUnknown = true)
  data class GameworldCreatedEventPlanetDto(
    val id: UUID,
    val x: Int,
    val y: Int,
    val movementDifficulty: Int,
    val resource: PlanetResourceDto?
  )
}
