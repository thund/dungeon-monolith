package com.github.twobiers.dungeonmonolith.robot.domain.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.robot.Inventory
import java.util.*

data class RobotInventoryUpdated(
  private val playerId: UUID,
  val robot: UUID,
  val inventory: Inventory
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
