package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class PruneRobotsHandler(
  val robotRepository: RobotRepository
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle() {
    robotRepository.deleteAll()
  }
}
