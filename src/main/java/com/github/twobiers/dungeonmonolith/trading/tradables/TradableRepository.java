package com.github.twobiers.dungeonmonolith.trading.tradables;

import java.util.List;

public interface TradableRepository {
    Tradable findByName(String name);
    List<Tradable> findByType(TradableType type);
    List<Tradable> findAll();
}
