package com.github.twobiers.dungeonmonolith.game.aggregates.core

import java.time.Instant
import java.util.*

abstract class AbstractEvent (
    private val id: UUID,
    private val transactionId: UUID,
    private val occurredAt: Instant,
    private val eventName: String,
    private val topic: String,
    private val version: Int

) : Event {

    override fun getId(): UUID = id

    override fun getTransactionId(): UUID = transactionId

    override fun getOccurredAt(): Instant = occurredAt

    override fun getEventName(): String = eventName

    override fun getTopic(): String = topic

    override fun getVersion(): Int = version

    override fun isSameAs(event: Event): Boolean =
        getId() == event.getId()
                && getTransactionId() == event.getTransactionId()
                && getOccurredAt() == event.getOccurredAt()
                && getEventName() == event.getEventName()
                && getTopic() == event.getTopic()
                && getVersion() == event.getVersion()
}
