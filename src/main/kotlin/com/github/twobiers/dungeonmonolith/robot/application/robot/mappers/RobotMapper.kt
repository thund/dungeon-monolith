package com.github.twobiers.dungeonmonolith.robot.application.robot.mappers

import com.github.twobiers.dungeonmonolith.robot.application.robot.dtos.InventoryDto
import com.github.twobiers.dungeonmonolith.robot.application.robot.dtos.RobotDto
import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import com.github.twobiers.dungeonmonolith.robot.domain.robot.Inventory
import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper
abstract class RobotMapper {

  @Mappings(
    Mapping(target = "planet", source = "planet.planetId"),
    Mapping(target = "inventory", source = "robot.inventory"),
    Mapping(target = "storageLevel", source = "robot.inventory.storageLevel"),
  )
  abstract fun robotToRobotDto(robot: Robot): RobotDto

  abstract fun robotsToRobotDtos(robots: List<Robot>): List<RobotDto>

  fun getInventory(inventory: Inventory): InventoryDto {
    return InventoryDto(
      inventory.maxStorage,
      inventory.usedStorage,
      inventory.getStorageUsageForResource(ResourceType.COAL),
      inventory.getStorageUsageForResource(ResourceType.IRON),
      inventory.getStorageUsageForResource(ResourceType.GEM),
      inventory.getStorageUsageForResource(ResourceType.GOLD),
      inventory.getStorageUsageForResource(ResourceType.PLATIN)
    )
  }
}
