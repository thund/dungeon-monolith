package com.github.twobiers.dungeonmonolith.game.aggregates.player.controller.dtos

import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.Player
import java.util.*

data class CreatePlayerRequestDto(
    val name: String,
    val email: String
) {

}
