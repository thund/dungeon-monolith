package com.github.twobiers.dungeonmonolith.robot.infrastructure.trading.dto

enum class TradableType {
  RESOURCE,
  UPGRADE,
  RESTORATION,
  ITEM
}
