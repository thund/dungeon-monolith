package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotResourceRemovedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class ResourceRemovalHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }
  fun handle(robotId: UUID, resourceType: ResourceType, amount: Int, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    robot.takeResources(resourceType, amount)
    this.robotRepository.save(robot)

    val event = RobotResourceRemovedIntegrationEvent.build(robot, resourceType, amount)
    this.integrationEventPublisher.publish(event, transactionId)
  }
}
