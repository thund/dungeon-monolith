package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class NoActiveGameClockException(override val message: String) : Exception(message) {
}
