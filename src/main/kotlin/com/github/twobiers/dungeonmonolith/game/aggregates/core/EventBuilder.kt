package com.github.twobiers.dungeonmonolith.game.aggregates.core

import com.github.twobiers.dungeonmonolith.game.aggregates.core.Event

interface EventBuilder {
    fun deserializedEvent(serialized: String): Event
}
