package com.github.twobiers.dungeonmonolith.robot.domain.core

/**
 * Marker interface for a domain event
 */
interface DomainEvent
