package com.github.twobiers.dungeonmonolith.robot.infrastructure.map

import com.github.twobiers.dungeonmonolith.robot.application.core.FailureException
import java.util.*

class NoResourceOnPlanetException(planetId: UUID) :
  FailureException("Map Service did not return any resource on the planet $planetId")
