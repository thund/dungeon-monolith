package com.github.twobiers.dungeonmonolith.map.application.commands;

import com.github.twobiers.dungeonmonolith.map.application.commands.dto.CreateGameworldDto;
import com.github.twobiers.dungeonmonolith.map.application.commands.dto.CreateGameworldResponseDto;
import com.github.twobiers.dungeonmonolith.map.domain.gameworld.GameworldRepository;
import com.github.twobiers.dungeonmonolith.map.domain.gameworld.GameworldStatus;
import com.github.twobiers.dungeonmonolith.map.domain.gameworld.factory.DefaultGameworldFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CreateNewGameworld {
  private final GameworldRepository gameworldRepository;

  @Transactional
  public CreateGameworldResponseDto create(CreateGameworldDto dto) {
    var factory = new DefaultGameworldFactory(dto.playerAmount());
    var gameworld = factory.create();
    gameworld.changeStatus(GameworldStatus.ACTIVE);

    var active = gameworldRepository.findAllActive();
    if (active.size() > 0) {
      active.forEach(g -> g.changeStatus(GameworldStatus.INACTIVE));
      gameworldRepository.saveAll(active);
    }

    gameworld = gameworldRepository.save(gameworld);
    return new CreateGameworldResponseDto(gameworld.getId());
  }
}
