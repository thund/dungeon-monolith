package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class GameStateException(override val message: String): Exception (message) {
}
