package com.github.twobiers.dungeonmonolith.map.domain.planet.event;

import com.github.twobiers.dungeonmonolith.map.domain.planet.Resource;
import java.util.UUID;

public record ResourceMined(
    UUID planet,
    int minedAmount,
    Resource resource
) {

}
