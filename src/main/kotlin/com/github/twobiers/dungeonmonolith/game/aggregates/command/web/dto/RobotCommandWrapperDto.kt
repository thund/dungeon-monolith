package com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto

data class RobotCommandWrapperDto(
    val commands: List<RobotCommandDto>
)
