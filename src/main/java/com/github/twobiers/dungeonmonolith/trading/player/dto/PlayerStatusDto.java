package com.github.twobiers.dungeonmonolith.trading.player.dto;

import java.util.UUID;

public record PlayerStatusDto(
    UUID playerId,
    UUID gameId,
    String name
) {}
