package com.github.twobiers.dungeonmonolith.game.aggregates.command.web

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto.BuyCommandDto
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto.SellCommandDto
import com.github.twobiers.dungeonmonolith.trading.tradables.TradableFacade
import jakarta.transaction.Transactional
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component

@Component
class TradingClient @Autowired constructor(
    private val tradableFacade: TradableFacade
) {
    companion object {
        private val logging = KotlinLogging.logger {}
    }

    @Transactional(Transactional.TxType.NOT_SUPPORTED)
    fun sendSellingCommands(commands: List<SellCommandDto>) {
        commands.forEach { command ->
            try {
                tradableFacade.sellInventory(command.payload.robotId)
            } catch (e: Exception) {
                logging.warn(e) { "Failed to sell inventory for robot ${command.payload.robotId}" }
            }
        }
    }

    @Transactional(Transactional.TxType.NOT_SUPPORTED)
    fun sendBuyingCommands(commands: List<BuyCommandDto>) {
        commands.forEach { command ->
            try {
                if (command.payload.robotId == null) {
                    tradableFacade.buyForPlayer(
                        command.playerId,
                        command.payload.itemName,
                        command.payload.amount
                    )
                } else {
                    tradableFacade.buyForRobot(
                        command.payload.robotId,
                        command.payload.itemName,
                        command.payload.amount
                    )
                }
            } catch (e: Exception) {
                logging.warn(e) { "Failed to buy for robot ${command.payload.robotId}" }
            }
        }
    }
}
