package com.github.twobiers.dungeonmonolith.robot.infrastructure.trading

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.robot.application.robot.RestorationType
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.ResourceRemovalHandler
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.RestorationHandler
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.SpawnRobotHandler
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.UpgradeRobotHandler
import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import com.github.twobiers.dungeonmonolith.robot.domain.robot.UpgradeType
import com.github.twobiers.dungeonmonolith.robot.infrastructure.trading.dto.TradableBoughtDto
import com.github.twobiers.dungeonmonolith.robot.infrastructure.trading.dto.TradableSoldDto
import com.github.twobiers.dungeonmonolith.robot.infrastructure.trading.dto.TradableType
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import java.util.*

@Component
class TradingEventListener(
  val resourceRemovalHandler: ResourceRemovalHandler,
  val restorationHandler: RestorationHandler,
  val spawnRobotHandler: SpawnRobotHandler,
  val upgradeRobotHandler: UpgradeRobotHandler,
  val objectMapper: ObjectMapper
) {
  val logger = KotlinLogging.logger { }

  @KafkaListener(topics = ["trade-sell"], groupId = "robot", autoStartup = "true")
  fun tradeSellListener(
    @Header("transactionId", required = false) transactionIdStr: String? = null,
    record: ConsumerRecord<String, String>
  ) {
    val event = objectMapper.readValue(record.value(), TradableSoldDto::class.java)
    val transactionId = transactionIdStr?.let { UUID.fromString(it) }
    when (event.type) {
      TradableType.RESOURCE -> handleResourceSold(event, transactionId)
      else -> logger.debug { "Not implemented yet: ${event.type}" }
    }
  }

  @KafkaListener(topics = ["trade-buy"], groupId = "robot", autoStartup = "true")
  fun tradeBuyListener(
    @Header("transactionId", required = false) transactionIdStr: String? = null,
            record: ConsumerRecord<String, String>
  ) {
    val event = objectMapper.readValue(record.value(), TradableBoughtDto::class.java)
    val transactionId = transactionIdStr?.let { UUID.fromString(it) }
    for (i in 1..event.amount) {
      when (event.type) {
        TradableType.ITEM -> handleItemBought(event, transactionId)
        TradableType.RESTORATION -> handleRestorationBought(event, transactionId)
        TradableType.UPGRADE -> handleUpgradeBought(event, transactionId)
        else -> logger.debug { "Not implemented yet: ${event.type}" }
      }
    }
  }

  private fun handleItemBought(event: TradableBoughtDto, transactionId: UUID? = null) {
    when (event.name.uppercase()) {
      "ROBOT" -> {
        spawnRobotHandler.handle(event.playerId, transactionId)
      }
      else -> logger.debug { "Not implemented yet: ${event.name}" }
    }
  }

  private fun handleUpgradeBought(event: TradableBoughtDto, transactionId: UUID? = null) {
    val upgradeType = UpgradeType.valueOf(event.name.uppercase().substringBeforeLast("_"))
    val level = event.name.substringAfterLast("_").toInt()
    if (event.robotId == null) {
      throw RuntimeException("RobotId is null")
    }

    upgradeRobotHandler.handle(event.robotId, upgradeType, level, transactionId)
  }

  private fun handleRestorationBought(event: TradableBoughtDto, transactionId: UUID? = null) {
    if (event.robotId == null) {
      throw RuntimeException("RobotId is null")
    }
    when (event.name.uppercase()) {
      "HEALTH_RESTORE" -> {
        restorationHandler.handle(event.robotId, RestorationType.HEALTH, transactionId)
      }
      "ENERGY_RESTORE" -> {
        restorationHandler.handle(event.robotId, RestorationType.ENERGY, transactionId)
      }
      else -> logger.debug { "Not implemented yet: ${event.name}" }
    }
  }

  private fun handleResourceSold(event: TradableSoldDto, transactionId: UUID? = null) {
    val resource = ResourceType.valueOf(event.name.uppercase())
    resourceRemovalHandler.handle(event.robotId, resource, event.amount, transactionId)
  }
}
