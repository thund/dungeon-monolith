package com.github.twobiers.dungeonmonolith.game.aggregates.command.web

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.robot.application.command.RobotCommand
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.MediatingCommandDispatcher
import jakarta.transaction.Transactional
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class RobotClient @Autowired constructor(
  private val mediatingCommandDispatcher: MediatingCommandDispatcher
) {
    @Transactional(Transactional.TxType.NOT_SUPPORTED)
  fun sendCommands(commands: List<Command>) {
    if (commands.isEmpty()) {
      return
    }

    val robotCommands = commands.map {
      RobotCommand(
        it.getRobot()!!.getRobotId(),
        it.getCommandId(),
        it.getPlayer().getPlayerId(),
        RobotCommand.CommandType.valueOf(it.getCommandType().name),
        it.getCommandPayload()?.getPlanetId(),
        it.getCommandPayload()?.getTargetId()
      )
    }

    mediatingCommandDispatcher.handleList(robotCommands)
  }
}
