package com.github.twobiers.dungeonmonolith.robot.domain.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import java.util.*

data class RobotResourceMined(
  private val playerId: UUID,
  val robot: UUID,
  val resourceType: ResourceType,
  val amount: Int
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
