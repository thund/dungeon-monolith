package com.github.twobiers.dungeonmonolith.robot.domain.core

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.domain.AfterDomainEventPublication
import org.springframework.data.domain.DomainEvents
import jakarta.persistence.Transient

/**
 * Base aggregate root
 *
 * @property events a transient list that contains domain events fired by the entity
 */
abstract class AbstractAggregateRoot(
  @Transient
  @kotlin.jvm.Transient
  @JsonIgnore
  protected val events: MutableList<DomainEvent> = mutableListOf()
) {

  @DomainEvents
  fun domainEvents(): Collection<DomainEvent> {
    return events.toList()
  }

  @AfterDomainEventPublication
  fun clearEvents() {
    this.events.clear()
  }
}
