package com.github.twobiers.dungeonmonolith.robot.application.robot

enum class RestorationType {
  HEALTH, ENERGY
}
