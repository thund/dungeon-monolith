package com.github.twobiers.dungeonmonolith.robot.infrastructure.game

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.PruneRobotsHandler
import com.github.twobiers.dungeonmonolith.robot.application.robot.handlers.RevealRobotsHandler
import com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto.GameStatus
import com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto.GameStatusEvent
import com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto.RoundStatus
import com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto.RoundStatusEvent
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component

@Component
class GameEventListener(
  val pruneRobotsHandler: PruneRobotsHandler,
  val revealRobotsHandler: RevealRobotsHandler,
  val objectMapper: ObjectMapper
) {
  val logger = KotlinLogging.logger { }

  @KafkaListener(topics = ["status"], groupId = "robot", autoStartup = "true")
  fun gameStatusListener(
    record: ConsumerRecord<String, String>
  ) {
    val event = objectMapper.readValue(record.value(), GameStatusEvent::class.java)
    if(event.status == GameStatus.ENDED) {
      pruneRobotsHandler.handle()
    }

  }

  @KafkaListener(topics = ["roundStatus"], groupId = "robot", autoStartup = "true")
  fun roundStatusListener(
    record: ConsumerRecord<String, String>
  ) {
    val event = objectMapper.readValue(record.value(), RoundStatusEvent::class.java)
    if(event.roundStatus == RoundStatus.ENDED) {
      revealRobotsHandler.handle()
    }
  }

}
