package com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto

import java.util.UUID

data class NewGameWorldResponseDto(
    val gameworldId: UUID
)
