package com.github.twobiers.dungeonmonolith.robot.application.planet

import com.github.twobiers.dungeonmonolith.robot.domain.planet.RobotPlanet
import com.github.twobiers.dungeonmonolith.robot.domain.planet.PlanetType
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper(componentModel = "spring")
abstract class PlanetMapper {

  @Mappings(
    Mapping(target = "planetId", source = "planet.planetId"),
    Mapping(target = "movementDifficulty", source = "movementCost"),
    Mapping(target = "planetType", source = "planetType"),
    Mapping(target = "resourceType", source = "planet.resourceType")
  )
  abstract fun planetToPlanetDTO(
    planet: RobotPlanet,
    movementCost: Int,
    planetType: PlanetType
  ): PlanetDto
}
