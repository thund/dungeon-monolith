package com.github.twobiers.dungeonmonolith.map.application.event.outgoing;

import com.github.twobiers.dungeonmonolith.map.domain.primitive.Direction;
import java.util.UUID;

public record PlanetNeighbourDto(
    UUID id,
    Direction direction
) {

}
