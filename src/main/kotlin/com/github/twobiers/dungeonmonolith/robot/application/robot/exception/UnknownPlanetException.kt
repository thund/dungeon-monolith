package com.github.twobiers.dungeonmonolith.robot.application.robot.exception

import com.github.twobiers.dungeonmonolith.robot.application.core.FailureException
import java.util.*

class UnknownPlanetException(planetId: UUID) :
  FailureException("Planet with ID $planetId not found")
