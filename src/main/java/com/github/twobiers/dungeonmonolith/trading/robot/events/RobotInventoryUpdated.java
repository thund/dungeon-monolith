package com.github.twobiers.dungeonmonolith.trading.robot.events;

import java.util.Map;
import java.util.UUID;

public record RobotInventoryUpdated(
    UUID robotId,
    Map<String, Integer> inventory
) {

}
