package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import com.github.twobiers.dungeonmonolith.robot.domain.robot.UpgradeType
import java.util.UUID

data class RobotUpgradedIntegrationEvent(
  val robotId: UUID,
  val upgrade: UpgradeType,
  val level: Int,
  val robot: Robot
) : RobotIntegrationEvent {
  companion object {
    fun build(robot: Robot, type: UpgradeType, level: Int) =
      RobotUpgradedIntegrationEvent(
        robotId = robot.id,
        upgrade = type,
        level = level,
        robot = robot
      )
  }

  override fun key(): String = robotId.toString()
}
