package com.github.twobiers.dungeonmonolith.global;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.kotlin.KotlinModule;
import com.github.twobiers.dungeonmonolith.robot.infrastructure.game.dto.GameStatusEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class JacksonConfig {
  @Bean
  public JavaTimeModule javaTimeModule() {
    return new JavaTimeModule();
  }

  @Bean
  public KotlinModule kotlinModule() {
    return new KotlinModule.Builder()
        .build();
  }

//  @Bean
//  public ObjectReader commandRequestReader(ObjectMapper mapper) {
//    return mapper.readerFor(
//        GameStatusEvent.class
//    )
//        .with(
//        DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
//        DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
//    );
//  }
}
