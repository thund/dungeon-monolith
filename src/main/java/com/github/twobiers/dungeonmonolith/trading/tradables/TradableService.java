package com.github.twobiers.dungeonmonolith.trading.tradables;

import java.util.UUID;

public interface TradableService {

    void buy(UUID playerId, UUID robotId, String name, Integer amount);

    void sell(UUID playerId, UUID robotId, String name, Integer amount);

}
