package com.github.twobiers.dungeonmonolith.trading.robot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotInventoryUpdatedDto;
import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotKilledDto;
import com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming.RobotSpawnedDto;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RobotEventConsumer {

    private final RobotService robotService;

    private final ObjectMapper objectMapper;

    public RobotEventConsumer(
        RobotService robotService, ObjectMapper objectMapper) {
        this.robotService = robotService;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "robot", groupId = "trading", autoStartup = "true")
    public void listenToRobotEvents(@Header("type") String type, ConsumerRecord<String, String> record) {
        switch (type) {
            case "RobotSpawned" -> handleRobotSpawned(record);
            case "RobotKilled" -> handleRobotKilled(record);
            case "RobotInventoryUpdated" -> handleInventoryUpdated(record);
        }
    }

    private void handleRobotSpawned(ConsumerRecord<String, String> record) {
        try {
            var event = objectMapper.readValue(record.value(), RobotSpawnedDto.class);
            robotService.create(event);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleRobotKilled(ConsumerRecord<String, String> record) {
        try {
            var event = objectMapper.readValue(record.value(), RobotKilledDto.class);
            robotService.delete(event.robot());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleInventoryUpdated(ConsumerRecord<String, String> record) {
        try {
            var event = objectMapper.readValue(record.value(), RobotInventoryUpdatedDto.class);
            robotService.updateInventory(event);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
