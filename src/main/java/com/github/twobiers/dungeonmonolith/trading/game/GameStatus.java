package com.github.twobiers.dungeonmonolith.trading.game;

import com.fasterxml.jackson.annotation.JsonAlias;

public enum GameStatus {
    @JsonAlias("ended")
    ENDED,
    @JsonAlias("created")
    CREATED,
    @JsonAlias("started")
    STARTED
}
