package com.github.twobiers.dungeonmonolith.game.aggregates.command.event

import com.fasterxml.jackson.annotation.JsonUnwrapped
import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.core.EventDto

class CommandCreatedDto(
    @JsonUnwrapped
    val command: Command
) : EventDto
