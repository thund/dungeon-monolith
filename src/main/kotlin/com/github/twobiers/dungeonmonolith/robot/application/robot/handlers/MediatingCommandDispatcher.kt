package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.command.RobotCommand
import com.github.twobiers.dungeonmonolith.robot.application.event.InternalErrorEvent
import jakarta.transaction.Transactional
import mu.KotlinLogging
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service

@Service
class MediatingCommandDispatcher(
  private val fightingCommandHandler: FightingCommandHandler,
  private val miningCommandHandler: MiningCommandHandler,
  private val movementCommandHandler: MovementCommandHandler,
  private val regenerateCommandHandler: RegenerateCommandHandler,
  private val applicationEventPublisher: ApplicationEventPublisher
) {

  companion object {
    val logger = KotlinLogging.logger {}
  }

  // Since we are dispatching commands in-process in the monolith we can leverage transaction
  // boundaries for a little performance boost.
  @Transactional
  fun handleList(commands: List<RobotCommand>) {
    commands.forEach {
      try {
        handle(it)
      } catch (e: Exception) {
        // We need to ensure that every exception is caught, otherwise the whole command list fails
        logger.error(e) { "Error while executing command: $it" }
        this.applicationEventPublisher.publishEvent(
          InternalErrorEvent(
            it.playerId,
            it.transactionId,
            it.robotId,
            e.message ?: "Unhandled error while executing command",
            e.stackTraceToString()
          )
        )
      }
    }
  }

  private fun handle(command: RobotCommand) {
    // Exhaustive pattern matching for enums will be default in Kotlin 1.7.
    //  Therefore we will get compile time errors if we forget to handle a command type.
    when (command.command) {
      RobotCommand.CommandType.BATTLE -> fightingCommandHandler.handle(command)
      RobotCommand.CommandType.MINING -> miningCommandHandler.handle(command)
      RobotCommand.CommandType.MOVEMENT -> movementCommandHandler.handle(command)
      RobotCommand.CommandType.REGENERATE -> regenerateCommandHandler.handle(command)
    }
  }
}
