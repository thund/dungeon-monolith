package com.github.twobiers.dungeonmonolith.robot.domain.robot.event

import java.util.*

data class RobotHealthUpdated(
  private val playerId: UUID,
  val robot: UUID,
  val amount: Int,
  val health: Int
) : RobotDomainEvent {
  override fun robotId(): UUID = robot
  override fun playerIds(): List<UUID> = listOf(playerId)
}
