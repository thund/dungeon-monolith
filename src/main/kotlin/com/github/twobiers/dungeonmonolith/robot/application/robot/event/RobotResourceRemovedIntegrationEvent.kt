package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType
import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import java.util.UUID

data class RobotResourceRemovedIntegrationEvent(
  val robotId: UUID,
  val removedResource: ResourceType,
  val removedAmount: Int,
  val resourceInventory: Map<ResourceType, Int>
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot, removedResource: ResourceType, removedAmount: Int) =
      RobotResourceRemovedIntegrationEvent(
        robotId = robot.id,
        removedResource = removedResource,
        removedAmount = removedAmount,
        resourceInventory = robot.inventory.resources
      )
  }

  override fun key(): String = robotId.toString()
}
