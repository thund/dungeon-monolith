package com.github.twobiers.dungeonmonolith.trading.bank;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BankAccountRepository extends CrudRepository<BankAccount, UUID> {
    @Query("select b from BankAccount b inner join b.player p where p.id = ?1")
    Optional<BankAccount> findBankAccountByPlayerId(UUID playerId);
}
