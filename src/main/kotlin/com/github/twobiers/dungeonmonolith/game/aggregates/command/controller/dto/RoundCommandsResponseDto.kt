package com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.Command
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.Round
import java.util.*

class RoundCommandsResponseDto (
    val gameId: UUID,
    val roundId: UUID,
    val roundNumber: Int,
    val commands: List<RoundCommandResponseDto>
) {
    constructor(round: Round, commands: List<Command>): this (
        gameId = round.getGameId(),
        roundId = round.getRoundId(),
        roundNumber = round.getRoundNumber(),
        commands.map { RoundCommandResponseDto(round.getGameId(), it) }
    )
}
