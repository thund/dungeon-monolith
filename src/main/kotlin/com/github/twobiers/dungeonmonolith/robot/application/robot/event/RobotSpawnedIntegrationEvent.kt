package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot

data class RobotSpawnedIntegrationEvent(
  val robot: Robot,
) : RobotIntegrationEvent {

  companion object {
    fun build(robot: Robot) =
      RobotSpawnedIntegrationEvent(
        robot = robot
      )
  }

  override fun key(): String = robot.id.toString()
}
