package com.github.twobiers.dungeonmonolith.game.aggregates.command.web.dto

class NewGameWorldDto(
    val playerAmount: Int
) {
    companion object {
        fun makeFromNumberOfPlayer(numberOfPlayer: Int): NewGameWorldDto {
            return NewGameWorldDto(numberOfPlayer)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as NewGameWorldDto

        if (playerAmount != other.playerAmount) return false

        return true
    }

    override fun hashCode(): Int {
        return playerAmount
    }


}
