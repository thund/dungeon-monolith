package com.github.twobiers.dungeonmonolith.robot.application.core

class DungeonMessageHeaders {
  companion object {
    const val TRANSACTION_ID: String = "transactionId"
    const val PLAYER_ID: String = "playerId"
    const val VERSION: String = "version"
    const val TIMESTAMP: String = "timestamp"
    const val EVENT_ID: String = "eventId"
    const val TYPE: String = "type"
  }
}
