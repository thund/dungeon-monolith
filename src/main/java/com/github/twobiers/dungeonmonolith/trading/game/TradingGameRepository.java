package com.github.twobiers.dungeonmonolith.trading.game;

import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface TradingGameRepository extends CrudRepository<TradingGame, UUID> {
    Optional<TradingGame> findByIsCurrentGame(Boolean bool);
}
