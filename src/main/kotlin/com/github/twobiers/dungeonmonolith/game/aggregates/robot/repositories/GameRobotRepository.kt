package com.github.twobiers.dungeonmonolith.game.aggregates.robot.repositories

import com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain.GameRobot
import org.springframework.data.repository.CrudRepository
import java.util.*

interface GameRobotRepository: CrudRepository<GameRobot, UUID> {}
