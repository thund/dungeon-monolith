package com.github.twobiers.dungeonmonolith.game.aggregates.player.domain

class PlayerAlreadyExistsException(override val message: String): Exception(message) {
}
