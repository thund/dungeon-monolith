package com.github.twobiers.dungeonmonolith.robot.domain.planet.event

import com.github.twobiers.dungeonmonolith.robot.domain.planet.RobotPlanet
import java.util.*

data class PlanetNeighbourRemoved(
  val planet: UUID,
  val neighbour: RobotPlanet
) : PlanetDomainEvent
