package com.github.twobiers.dungeonmonolith.trading.robot.dto.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotSpawnedDto(
    RobotDto robot
) { }
