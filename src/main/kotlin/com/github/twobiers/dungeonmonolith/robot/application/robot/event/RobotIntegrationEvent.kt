package com.github.twobiers.dungeonmonolith.robot.application.robot.event

sealed interface RobotIntegrationEvent {
  fun key(): String
}
