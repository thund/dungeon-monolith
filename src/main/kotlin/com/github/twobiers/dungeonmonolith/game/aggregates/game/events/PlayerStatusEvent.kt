package com.github.twobiers.dungeonmonolith.game.aggregates.game.events

import com.github.twobiers.dungeonmonolith.game.aggregates.core.AbstractEvent
import com.github.twobiers.dungeonmonolith.game.aggregates.game.events.dto.PlayerStatusEventDto
import java.time.Instant
import java.util.*

class PlayerStatusEvent (
    id: UUID,
    transactionId: UUID,
    occurredAt: Instant,
    eventName: String,
    topic: String,
    version: Int,

    private val playerId: UUID,
    val gameId: UUID,
    val playerUsername: String,

    ) : AbstractEvent(
    id = id,
    transactionId = transactionId,
    occurredAt = occurredAt,
    eventName = eventName,
    topic = topic,
    version = version
){
    override fun toDTO(): PlayerStatusEventDto = PlayerStatusEventDto(playerId, gameId, playerUsername)
    override fun getPlayerId(): UUID? = playerId

    override fun equals(other: Any?): Boolean =
        (other is PlayerStatusEvent)
                && getId() == other.getId()
                && getTransactionId() == other.getTransactionId()
                && getOccurredAt() == other.getOccurredAt()
                && getEventName() == other.getEventName()
                && getTopic() == other.getTopic()
                && getVersion() == other.getVersion()
                && playerId == other.playerId
                && playerUsername == other.playerUsername
}
