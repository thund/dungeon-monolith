package com.github.twobiers.dungeonmonolith.game.aggregates.core

class MethodNotAllowedForStatusException(message: String): Exception(message)
