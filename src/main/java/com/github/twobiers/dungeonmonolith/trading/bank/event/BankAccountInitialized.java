package com.github.twobiers.dungeonmonolith.trading.bank.event;

import java.math.BigDecimal;
import java.util.UUID;

public record BankAccountInitialized(
    UUID playerId,
    BigDecimal balance
) implements BankAccountEvent {
    @Override
    public String key() {
        return playerId.toString();
    }
}
