package com.github.twobiers.dungeonmonolith.game.aggregates.core

import java.time.Instant

/**
 * Base class for error messages which will be returned to the client.
 */
data class ErrorDetails(
    val timestamp: Instant,
    val message: String,
    val details: String?,
    val cause: String?
) {
    constructor(message: String) : this(Instant.now(), message, null, null)
    constructor(message: String, details: String?) : this(Instant.now(), message, details, null)
    constructor(message: String, details: String?, cause: String) : this(Instant.now(), message, details, cause)
}
