package com.github.twobiers.dungeonmonolith.trading.player;

import com.github.twobiers.dungeonmonolith.trading.player.events.PlayerCreated;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "trading_player")
public class TradingPlayer extends AbstractAggregateRoot<TradingPlayer> {
    @Id
    private UUID id;

    @Setter
    private String name;

    public TradingPlayer(UUID id, String name) {
        this.id = id;
        this.name = name;
        this.registerEvent(new PlayerCreated(id, name));
    }
}
