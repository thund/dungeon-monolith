package com.github.twobiers.dungeonmonolith.robot.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.github.twobiers.dungeonmonolith.robot.domain.planet.ResourceType

@JsonIgnoreProperties(ignoreUnknown = true)
data class PlanetResourceDto(
  val resourceType: ResourceType,
  val maxAmount: Int,
  val currentAmount: Int
)
