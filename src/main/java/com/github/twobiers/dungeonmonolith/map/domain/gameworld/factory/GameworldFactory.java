package com.github.twobiers.dungeonmonolith.map.domain.gameworld.factory;

import com.github.twobiers.dungeonmonolith.map.domain.gameworld.Gameworld;

public interface GameworldFactory {
  Gameworld create();
}
