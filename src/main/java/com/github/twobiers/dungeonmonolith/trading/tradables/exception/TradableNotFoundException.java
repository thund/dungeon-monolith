package com.github.twobiers.dungeonmonolith.trading.tradables.exception;

public class TradableNotFoundException extends RuntimeException {
    public TradableNotFoundException(String message) {
        super(message);
    }
}
