package com.github.twobiers.dungeonmonolith.game.aggregates.command.controller.dto

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandType
import java.util.*

class CommandRequestDto(
    val gameId: UUID,
    val playerId: UUID,
    val robotId: UUID?,
    val commandType: CommandType,
    val commandObject: CommandObjectRequestDto
) {
    override fun toString(): String =
        "CommandRequestDto(gameId=${gameId}, playerId=${"XXX"}, robotId=${robotId}, commandType='${commandType}', " +
        "commandObject=${commandObject})"

    override fun equals(other: Any?): Boolean =
        (other is CommandRequestDto)
                && gameId == other.gameId
                && robotId == other.robotId
                && commandType == other.commandType
                && commandObject == other.commandObject
}
