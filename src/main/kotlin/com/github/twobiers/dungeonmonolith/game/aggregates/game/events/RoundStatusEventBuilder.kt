package com.github.twobiers.dungeonmonolith.game.aggregates.game.events

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.game.aggregates.core.Event
import com.github.twobiers.dungeonmonolith.game.aggregates.core.EventBuilder
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.RoundStatus
import com.github.twobiers.dungeonmonolith.game.aggregates.game.services.GameLoopTimingPredictions
import com.github.twobiers.dungeonmonolith.game.aggregates.game.services.GameLoopTimings
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class RoundStatusEventBuilder @Autowired constructor(
) : EventBuilder {
    private val topic: String = "roundStatus"
    private val eventType: String = "round-status"
    private val version: Int = 1

    companion object {
        val objectMapper: ObjectMapper = ObjectMapper().findAndRegisterModules()
    }

    override fun deserializedEvent(serialized: String): Event {
        return objectMapper.readValue(serialized, RoundStatusEvent::class.java)
    }

    fun makeRoundStatusEvent(
        transactionId: UUID,
        gameId: UUID,
        roundId: UUID,
        roundNumber: Int,
        roundStatus: RoundStatus,
        timings: GameLoopTimings,
        predictions: GameLoopTimingPredictions?
    ): RoundStatusEvent {
        return RoundStatusEvent(
            id = UUID.randomUUID(),
            transactionId = transactionId,
            occurredAt = Instant.now(),
            eventName = eventType,
            topic = topic,
            version = version,
            gameId = gameId,
            roundId = roundId,
            roundNumber = roundNumber,
            roundStatus = roundStatus,
            timings = timings,
            timingPredictions = predictions
        )
    }
}
