package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class GameParticipationException(override val message: String) : Exception(message) {
}
