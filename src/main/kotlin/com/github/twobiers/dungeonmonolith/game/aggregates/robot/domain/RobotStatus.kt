package com.github.twobiers.dungeonmonolith.game.aggregates.robot.domain

enum class RobotStatus {
    ACTIVE,
    INACTIVE
}
