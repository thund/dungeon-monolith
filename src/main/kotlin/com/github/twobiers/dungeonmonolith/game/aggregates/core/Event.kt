package com.github.twobiers.dungeonmonolith.game.aggregates.core

import java.time.Instant
import java.util.*


interface Event {
    fun getId(): UUID

    fun getTransactionId(): UUID

    fun getEventName(): String

    fun getVersion(): Int

    fun getOccurredAt(): Instant

    fun getTopic(): String

    fun toDTO(): EventDto

    fun isSameAs(event: Event): Boolean

    fun getPlayerId(): UUID?
}
