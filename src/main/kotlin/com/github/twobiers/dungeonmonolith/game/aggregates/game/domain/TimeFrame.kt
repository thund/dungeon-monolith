package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

import java.time.Duration
import java.time.temporal.ChronoUnit

class TimeFrame (
    private val roundDuration: Duration
) {
    constructor(game: Game):
            this(Duration.of(game.getTotalRoundTimespanInMS(), ChronoUnit.MILLIS))

    fun commandInputTime(): Duration {
        return roundDuration.minus(executionTime())
    }

    fun executionTime(): Duration {
        return roundDuration
            .dividedBy(100)
            .multipliedBy(10)
    }

    fun danglingTime(): Duration {
        return roundDuration
            .minus(commandInputTime())
            .minus(executionTime())
    }
}
