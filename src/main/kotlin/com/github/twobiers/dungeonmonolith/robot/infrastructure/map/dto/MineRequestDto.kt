package com.github.twobiers.dungeonmonolith.robot.infrastructure.map.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class MineRequestDto(
  val amountToMine: Int
) {
}
