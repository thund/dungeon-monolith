package com.github.twobiers.dungeonmonolith.robot.domain.planet.event

import com.github.twobiers.dungeonmonolith.robot.domain.core.DomainEvent

sealed interface PlanetDomainEvent : DomainEvent
