package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotSpawnedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.domain.planet.RobotPlanetRepository
import com.github.twobiers.dungeonmonolith.robot.domain.robot.Robot
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class SpawnRobotHandler(
  val robotPlanetRepository: RobotPlanetRepository,
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(player: UUID, transactionId: UUID? = null) : Robot {
    val spawnPlanet = robotPlanetRepository.getRandomPlanet()
    val robot = Robot.of(player, spawnPlanet)

    logger.info("Spawned new robot with id ${robot.id} for player $player")

    this.robotRepository.save(robot)

    val event = RobotSpawnedIntegrationEvent.build(robot)
    this.integrationEventPublisher.publish(event, transactionId)

    return robot
  }
}
