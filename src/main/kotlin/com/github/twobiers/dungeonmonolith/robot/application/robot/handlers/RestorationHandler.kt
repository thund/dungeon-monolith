package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.robot.RestorationType
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotRestoredAttributesIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class RestorationHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(robotId: UUID, type: RestorationType, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    when (type) {
      RestorationType.ENERGY -> robot.restoreEnergy()
      RestorationType.HEALTH -> robot.repair()
    }
    this.robotRepository.save(robot)

    val event = RobotRestoredAttributesIntegrationEvent.build(robot, type)
    this.integrationEventPublisher.publish(event, transactionId)
  }
}
