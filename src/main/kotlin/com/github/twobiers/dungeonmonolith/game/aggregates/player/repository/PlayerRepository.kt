package com.github.twobiers.dungeonmonolith.game.aggregates.player.repository

import com.github.twobiers.dungeonmonolith.game.aggregates.player.domain.Player
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.CrudRepository
import java.util.*

interface PlayerRepository: JpaRepository<Player, UUID> {

    fun findByUserNameOrMailAddress(userName: String, mailAddress: String): Optional<Player>

    fun findByUserNameAndMailAddress(userName: String, mailAddress: String): Optional<Player>
}
