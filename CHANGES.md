- Dependency Upgrades
- RPC -> Method invocations
- Minimize Configuration (remove some configuration properties / usages) and hardcoding
- Rename beans to resolve conflicts
- Rename tables to resolve conflicts
- Using manual JSON Serialization for some Kafka listeners
- Prevent from errors being thrown and align with transaction management
  - In Microservices if a transaction fails it is isolated to that service. In a monolith it would 
    affect the whole transaction and maybe kill the game.
