package com.github.twobiers.dungeonmonolith.game.aggregates.command.domain

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

enum class CommandType {
    MINING,
    MOVEMENT,
    BATTLE,
    BUYING,
    SELLING,
    REGENERATE;

    @JsonValue
    open fun forJackson(): String? {
        // We need a lowercase serialization format to not trigger a breaking change in our players
        // TODO: We should make it consistent in the future.
        return name.lowercase(Locale.getDefault())
    }
}
