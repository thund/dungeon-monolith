package com.github.twobiers.dungeonmonolith.robot.application.robot.dtos

import com.github.twobiers.dungeonmonolith.robot.application.robot.RestorationType
import java.util.*

data class RestorationDto(
  val transactionId: UUID,
  val restorationType: RestorationType
)
