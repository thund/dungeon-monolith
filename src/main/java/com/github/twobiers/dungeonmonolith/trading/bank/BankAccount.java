package com.github.twobiers.dungeonmonolith.trading.bank;

import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountCleared;
import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountInitialized;
import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountTransactionBooked;
import com.github.twobiers.dungeonmonolith.trading.bank.exception.BalanceNotSufficientException;
import com.github.twobiers.dungeonmonolith.trading.player.TradingPlayer;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BankAccount extends AbstractAggregateRoot<BankAccount> {
    // We could also use some IBAN-Thing here^^
    @Id
    private UUID id = UUID.randomUUID();

    @OneToOne(optional = false)
    private TradingPlayer player;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<BankTransaction> transactions = new ArrayList<>();

    public BankAccount(TradingPlayer player) {
        Objects.requireNonNull(player, "The player must not be null.");

        this.player = player;
    }

    public BankAccount(TradingPlayer player, BigDecimal startBalance) {
        Objects.requireNonNull(player, "The player must not be null.");
        Objects.requireNonNull(startBalance, "Start balance must not be null.");

        if(startBalance.compareTo(BigDecimal.ZERO) != 0) {
            // Note we are not using the deposit() method as we want to be able to initialize a bank account
            //  with negative balance.
            this.transactions.add(new BankTransaction(LocalDateTime.now(), startBalance));
        }
        this.player = player;
    }

    public void initialize(BigDecimal amount) {
        this.transactions.clear();
        if(amount.compareTo(BigDecimal.ZERO) != 0) {
            // Note we are not using the deposit() method as we want to be able to initialize a bank account
            //  with negative balance.
            this.transactions.add(new BankTransaction(LocalDateTime.now(), amount));
        }
        this.registerEvent(new BankAccountInitialized(player.getId(), amount));
    }

    public void deposit(BigDecimal amount) {
        Objects.requireNonNull(amount, "Amount must not be null.");

        if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException("The amount of money must be greater than zero.");
        this.transactions.add(new BankTransaction(LocalDateTime.now(), amount));
        this.registerEvent(new BankAccountTransactionBooked(player.getId(), amount, this.getBalance()));
    }

    /**
     *
     * @throws BalanceNotSufficientException if the balance is not sufficient to withdraw the amount.
     * @param amount
     */
    public void withdraw(BigDecimal amount) {
        Objects.requireNonNull(amount, "Amount must not be null.");

        if (amount.compareTo(this.getBalance()) > 0)
            throw new BalanceNotSufficientException("The balance is not sufficient to withdraw the amount.");
        if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException("The amount of money must be greater than zero.");
        amount = amount.negate();
        this.transactions.add(new BankTransaction(LocalDateTime.now(), amount));
        this.registerEvent(new BankAccountTransactionBooked(player.getId(), amount, this.getBalance()));
    }

    public BigDecimal getBalance() {
        return this.transactions.stream()
            .sorted(Comparator.comparing(BankTransaction::getTimestamp))
            .reduce(BigDecimal.ZERO, (t1, t2) -> t1.add(t2.getAmount()), BigDecimal::add);
    }

    public List<BankTransaction> getTransactions() {
        // Immutable
        return List.copyOf(transactions);
    }

    public void reset() {
        this.transactions.clear();
        this.registerEvent(new BankAccountCleared(player.getId(), this.getBalance()));
    }
}
