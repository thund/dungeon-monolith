package com.github.twobiers.dungeonmonolith.robot.application.robot.event

import java.util.UUID

interface RobotIntegrationEventPublisher {
    fun publish(
      event: RobotIntegrationEvent,
      transactionId: UUID? = null,
    )
}
