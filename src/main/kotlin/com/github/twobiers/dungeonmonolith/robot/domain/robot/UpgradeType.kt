package com.github.twobiers.dungeonmonolith.robot.domain.robot

enum class UpgradeType {
  STORAGE,
  HEALTH,
  DAMAGE,
  MINING_SPEED,
  MINING,
  MAX_ENERGY,
  ENERGY_REGEN
}
