package com.github.twobiers.dungeonmonolith.trading.core.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic bank() {
        return new NewTopic("bank", 1, (short) 1);
    }

    @Bean
    public NewTopic prices() {
        return new NewTopic("prices", 1, (short) 1);
    }

    @Bean
    public NewTopic tradeSell() {
        return new NewTopic("trade-sell", 1, (short) 1);
    }

    @Bean
    public NewTopic tradeBuy() {
        return new NewTopic("trade-buy", 1, (short) 1);
    }
}
