package com.github.twobiers.dungeonmonolith.robot.domain.planet

import com.fasterxml.jackson.annotation.JsonIgnore
import com.github.twobiers.dungeonmonolith.robot.domain.core.AbstractAggregateRoot
import com.github.twobiers.dungeonmonolith.robot.domain.planet.event.PlanetDiscovered
import com.github.twobiers.dungeonmonolith.robot.domain.planet.event.PlanetNeighbourAdded
import com.github.twobiers.dungeonmonolith.robot.domain.planet.event.PlanetNeighbourRemoved
import java.util.*
import jakarta.persistence.*

@Entity
@Table(name = "robot_planets")
data class RobotPlanet private constructor(
  @Id
  
  val planetId: UUID,
  
  val gameWorldId: UUID,
  var movementDifficulty: Int = 1,
  var resourceType: ResourceType? = null,
  @JsonIgnore
  @ManyToMany(fetch = FetchType.EAGER)
  var neighbours: MutableList<RobotPlanet> = mutableListOf()
) : AbstractAggregateRoot() {
  companion object {
    /**
     * Fabric method to create a new planet.
     * Adds discovering event.
     */
    fun of(
      planetId: UUID,
      gameWorldId: UUID,
      movementDifficulty: Int = 1,
      resourceType: ResourceType? = null,
      neighbours: MutableList<RobotPlanet> = mutableListOf()
    ): RobotPlanet {
      val robotPlanet = RobotPlanet(planetId, gameWorldId, movementDifficulty, resourceType, neighbours)
      robotPlanet.events.add(PlanetDiscovered(robotPlanet))
      return robotPlanet
    }
  }

  fun addNeighbour(robotPlanet: RobotPlanet) {
    this.neighbours.add(robotPlanet)
    this.events.add(PlanetNeighbourAdded(planetId, robotPlanet))
  }

  fun removeNeighbour(robotPlanet: RobotPlanet) {
    this.neighbours.removeIf { it == robotPlanet || it.planetId == robotPlanet.planetId }
    this.events.add(PlanetNeighbourRemoved(planetId, robotPlanet))
  }
  fun isNeighbourTo(planetId: UUID) = this.neighbours.any { it.planetId == planetId }
  override fun toString(): String {
    return "Planet(planetId=$planetId, gameWorldId=$gameWorldId, movementDifficulty=$movementDifficulty, resourceType=$resourceType, neighbours=${neighbours.map { it.planetId }})"
  }
}
