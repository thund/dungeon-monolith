package com.github.twobiers.dungeonmonolith.game.aggregates.game.services

import com.github.twobiers.dungeonmonolith.game.aggregates.command.domain.CommandType
import com.github.twobiers.dungeonmonolith.game.aggregates.command.services.CommandService
import com.github.twobiers.dungeonmonolith.game.aggregates.command.web.MapClient
import com.github.twobiers.dungeonmonolith.game.aggregates.eventpublisher.EventPublisher
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.*
import com.github.twobiers.dungeonmonolith.game.aggregates.game.events.GameStatusEvent
import com.github.twobiers.dungeonmonolith.game.aggregates.game.events.GameStatusEventBuilder
import com.github.twobiers.dungeonmonolith.game.aggregates.game.events.RoundStatusEvent
import com.github.twobiers.dungeonmonolith.game.aggregates.game.events.RoundStatusEventBuilder
import com.github.twobiers.dungeonmonolith.game.aggregates.game.repositories.GameRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*
import kotlin.concurrent.thread

@Service
class GameLoopService(
    private val roundStatusEventBuilder: RoundStatusEventBuilder,
    private val gameStatusEventBuilder: GameStatusEventBuilder,
    private val eventPublisher: EventPublisher,
    private val gameRepository: GameRepository,
    private val gameWorldsClient: MapClient,
    private val commandService: CommandService
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val gameClocks = mutableMapOf<UUID, GameLoopClock>()

    fun start(id: UUID) {
        val transactionId = UUID.randomUUID()

        val game = gameRepository.findById(id)
            .orElseThrow { GameNotFoundException("Failed to start game. Game with ID '$id' not found.") }

        if (gameRepository.existsByGameStatusIn(listOf(GameStatus.GAME_RUNNING))) {
            throw GameAlreadyActiveException("A new Game could not be started, because an active Game already exists.")
        }

        game.startGame()
        var response = gameWorldsClient.createNewGameWorld(game.getNumberJoinedPlayers())
        game.setGameworld(response.gameworldId)
        gameRepository.save(game)
        logger.info("Game started. [gameId=$id]")

        val clock = GameLoopClock(TimeFrame(game))
        gameClocks[id] = clock

        clock.onRoundEnd {
            game.endRound()
            gameRepository.save(game)
            publishRoundEvent(game.getCurrentRound()!!, it, clock.predictions)
        }
        clock.onRoundStart {
            try {
                game.startNewRound()
            }
            catch (e: MaximumRoundsReachedException) {
                logger.info { "Maximum rounds reached. Ending game." }
                this.end(id)
                return@onRoundStart
            }
            // I think when another exception occurs we have an inconsistent state and cannot continue the game.
            catch (e: Exception) {
                logger.debug("Failed to start next Round: {}", e.message)
                logger.debug("Stopping clock")
                removeGameClock(id)
                return@onRoundStart
            }
            gameRepository.save(game)
            publishRoundEvent(game.getCurrentRound()!!, it, clock.predictions)
        }
        clock.onCommandInputEnded {
            game.endCommandInputPhase()
            gameRepository.save(game)
            val round = game.getCurrentRound()!!
            executeCommandsInOrder(round.getRoundId())
            publishRoundEvent(round, it, clock.predictions)
        }

        // Thread will be interrupted when clock stops.
        thread(start = true, isDaemon = false) {
            Thread.sleep(1000)
            clock.run()
        }

        val gameStartedEvent: GameStatusEvent = gameStatusEventBuilder.makeGameStatusEvent(transactionId, game.getGameId(), GameStatus.GAME_RUNNING, game.getGameworldId())
        eventPublisher.publishEvent(gameStartedEvent)
    }

    fun end(id: UUID) {
        val transactionId = UUID.randomUUID()
        val game = gameRepository.findById(id)
            .orElseThrow { GameNotFoundException("Failed to end the game. Game with ID '$id' not found.") }

        game.endGame()
        gameRepository.save(game)

        removeGameClock(id)

        val gameEndedEvent: GameStatusEvent = gameStatusEventBuilder.makeGameStatusEvent(transactionId, game.getGameId(), GameStatus.GAME_FINISHED)
        eventPublisher.publishEvent(gameEndedEvent)
    }

    fun changeRoundDuration(gameId: UUID, duration: Long): UUID {
        val transactional = UUID.randomUUID()
        val game = gameRepository.findById(gameId).orElseThrow { GameNotFoundException("Game with ID '$gameId' not found.") }

        game.changeRoundDuration(duration)
        gameRepository.save(game)

        val clock = gameClocks[gameId];
        if (clock != null) {
            clock.patchTimeFrame(TimeFrame(game))
        } else {
            if(game.getGameStatus() == GameStatus.GAME_RUNNING) {
                throw NoActiveGameClockException("A Game clock for $gameId cannot be found. This should not occur unless the game is not created properly or maybe the service crashed?")
            }
        }
        logger.trace("Updated round duration. [newDurationInMillis=$duration, gameId=$gameId]")
        return transactional
    }

    private fun removeGameClock(gameId: UUID) {
        gameClocks[gameId]?.let { it.stop() }
        gameClocks.remove(gameId)
    }

    private fun executeCommandsInOrder(roundId: UUID) {
        logger.trace("Dispatching commands in order...")

        // This is our execution order. We need to execute the commands in this order.
        val types = listOf<CommandType>(
            CommandType.SELLING,
            CommandType.BUYING,
            CommandType.MOVEMENT,
            CommandType.BATTLE,
            CommandType.MINING,
            CommandType.REGENERATE
        )

        types.forEach {
            commandService.dispatchCommands(roundId, it)
        }

        logger.trace("Command dispatching completed.")
    }

    private fun publishRoundEvent(round: Round, timings: GameLoopTimings, predictions: GameLoopTimingPredictions?) {
        val transactionId = UUID.randomUUID()

        val roundEvent: RoundStatusEvent = roundStatusEventBuilder.makeRoundStatusEvent(
            transactionId, round.getGameId(), round.getRoundId(), round.getRoundNumber(), round.getRoundStatus(), timings, predictions
        )

        eventPublisher.publishEvent(roundEvent)

        logger.info("Round {} changed status to {}.", round.getRoundNumber(), round.getRoundStatus())
    }
}
