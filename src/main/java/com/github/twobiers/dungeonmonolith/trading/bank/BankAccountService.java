package com.github.twobiers.dungeonmonolith.trading.bank;

import java.math.BigDecimal;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public interface BankAccountService {
    void initializeForPlayer(UUID playerId, BigDecimal balance);
    BankAccount findByPlayerId(UUID playerId);
    void save(BankAccount bankAccount);
    void resetAllBankAccounts();
}
