package com.github.twobiers.dungeonmonolith.trading.bank;

import com.github.twobiers.dungeonmonolith.trading.bank.exception.NoBankAccountException;
import com.github.twobiers.dungeonmonolith.trading.player.PlayerService;
import java.math.BigDecimal;
import java.util.UUID;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BankAccountServiceImpl implements BankAccountService {

    private final BankAccountRepository bankAccountRepository;
    private final PlayerService playerService;

    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository,
        PlayerService playerService) {
        this.bankAccountRepository = bankAccountRepository;
        this.playerService = playerService;
    }

    public void initializeForPlayer(UUID playerId, BigDecimal balance) {
        var player = this.playerService.findPlayerByID(playerId);
        var bankAccount = this.bankAccountRepository.findBankAccountByPlayerId(player.getId())
            .orElse(new BankAccount(player, BigDecimal.ZERO));
        bankAccount.initialize(balance);
        this.bankAccountRepository.save(bankAccount);
    }

    /**
     * @throws NoBankAccountException if no bank account exists for the player
     * @param playerId ID
     * @return bank account
     */
    public BankAccount findByPlayerId(UUID playerId) {
        return this.bankAccountRepository.findBankAccountByPlayerId(playerId).orElseThrow(
            () -> new NoBankAccountException("Bank account not found for player " + playerId));
    }

    public void save(BankAccount bankAccount) {
        this.bankAccountRepository.save(bankAccount);
    }

    @Transactional
    public void resetAllBankAccounts() {
        var bankAccounts = this.bankAccountRepository.findAll();
        bankAccounts.forEach(BankAccount::reset);
        this.bankAccountRepository.saveAll(bankAccounts);
    }
}
