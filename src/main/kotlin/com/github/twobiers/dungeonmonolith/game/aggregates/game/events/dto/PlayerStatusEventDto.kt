package com.github.twobiers.dungeonmonolith.game.aggregates.game.events.dto

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.game.aggregates.core.EventDto
import java.util.*

class PlayerStatusEventDto (
    val playerId: UUID,
    val gameId: UUID,
    val name: String

) : EventDto {
}
