package com.github.twobiers.dungeonmonolith.trading.command;

public enum CommandType {
    BUY,
    SELL
}
