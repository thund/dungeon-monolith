package com.github.twobiers.dungeonmonolith.trading.tradables;

import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountDepositRequested;
import com.github.twobiers.dungeonmonolith.trading.bank.event.BankAccountWithdrawalRequested;
import com.github.twobiers.dungeonmonolith.trading.tradables.event.TradableBought;
import com.github.twobiers.dungeonmonolith.trading.tradables.event.TradableSold;
import com.github.twobiers.dungeonmonolith.trading.tradables.exception.TradableNotFoundException;
import jakarta.transaction.Transactional;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class TradableServiceImpl implements TradableService {
    private final TradableRepository tradableRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public TradableServiceImpl(TradableRepository tradableRepository,
        ApplicationEventPublisher applicationEventPublisher) {
        this.tradableRepository = tradableRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    @Transactional
    public void buy(UUID playerId, UUID robotId, String name, Integer amount) {
        if(amount <= 0)
            throw new IllegalArgumentException("Amount must be greater than 0");
        var tradable = getOrThrowTradable(name);

        var totalPrice = tradable.getPrice().multiply(BigDecimal.valueOf(amount));

        log.debug("Attempting to buy {} {} Tradables for a total price of {} for player {}", amount, name, totalPrice, playerId);

        if(totalPrice.compareTo(BigDecimal.ZERO) > 0) {
            applicationEventPublisher.publishEvent(new BankAccountWithdrawalRequested(playerId, totalPrice));
        }
        this.applicationEventPublisher.publishEvent(new TradableBought(playerId, robotId, tradable.getType(), name, amount, tradable.getPrice(), totalPrice));
    }

    @Override
    @Transactional
    public void sell(UUID playerId, UUID robotId, String name, Integer amount) {
        if(amount <= 0)
            throw new IllegalArgumentException("Amount must be greater than 0");
        var tradable = getOrThrowTradable(name);

        var totalPrice = tradable.getPrice().multiply(BigDecimal.valueOf(amount));

        log.debug("Attempting to sell {} {} Tradables for a total price of {} for player {}", amount, name, totalPrice, playerId);

        if(totalPrice.compareTo(BigDecimal.ZERO) > 0) {
            applicationEventPublisher.publishEvent(new BankAccountDepositRequested(playerId, totalPrice));
        }
        this.applicationEventPublisher.publishEvent(new TradableSold(playerId, robotId, tradable.getType(), name, amount, tradable.getPrice(), totalPrice));
    }

    private Tradable getOrThrowTradable(String name) {
        var tradable = tradableRepository.findByName(name);
        if(tradable == null) {
            throw new TradableNotFoundException("Tradable not found: " + name);
        }
        return tradable;
    }
}
