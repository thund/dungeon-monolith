package com.github.twobiers.dungeonmonolith.game.aggregates.game.controller.dto

import jakarta.validation.constraints.Min

class PatchGameMaxRoundsDto (
    @Min(1) val maxRounds: Int
)
