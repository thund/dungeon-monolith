package com.github.twobiers.dungeonmonolith.trading.player.events;

import java.util.UUID;

public record PlayerCreated(
    UUID id,
    String name
) {

}
