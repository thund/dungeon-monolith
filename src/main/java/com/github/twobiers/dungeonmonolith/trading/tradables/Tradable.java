package com.github.twobiers.dungeonmonolith.trading.tradables;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class Tradable {
    private final String name;
    private final BigDecimal price;
    private final TradableType type;

    public Tradable(String name, TradableType type, BigDecimal price) {
        this.name = name;
        this.price = price;
        this.type = type;
    }
}
