package com.github.twobiers.dungeonmonolith.trading.player;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface TradingPlayerRepository extends CrudRepository<TradingPlayer, UUID> {
}
