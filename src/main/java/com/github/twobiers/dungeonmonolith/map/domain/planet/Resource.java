package com.github.twobiers.dungeonmonolith.map.domain.planet;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import java.util.Objects;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Resource {
  @Enumerated(EnumType.STRING)
  private ResourceType type;
  private Integer maxAmount;
  private Integer currentAmount;

  public Resource(ResourceType type, int maxAmount) {
    Objects.requireNonNull(type);

    this.type = type;
    this.maxAmount = maxAmount;
    this.currentAmount = maxAmount;
  }

  public void setCurrentAmount(Integer currentAmount) {
    this.currentAmount = currentAmount;
  }
}
