package com.github.twobiers.dungeonmonolith.map.application.event.outgoing;

import com.github.twobiers.dungeonmonolith.map.domain.gameworld.GameworldStatus;
import java.util.List;
import java.util.UUID;

public record GameworldCreatedDto(
    UUID id,
    GameworldStatus status,
    List<FullPlanetDto> planets
) {

}
