package com.github.twobiers.dungeonmonolith.game.aggregates.game.repositories

import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.Game
import com.github.twobiers.dungeonmonolith.game.aggregates.game.domain.Round
import org.springframework.data.repository.CrudRepository
import java.util.*

interface RoundRepository : CrudRepository<Round, UUID> {

    fun findRoundByGameAndRoundNumber(game: Game, roundNumber: Int): Optional<Round>

    fun findRoundByGame_GameIdAndRoundNumber(gameId: UUID, roundNumber: Int): Optional<Round>
}
