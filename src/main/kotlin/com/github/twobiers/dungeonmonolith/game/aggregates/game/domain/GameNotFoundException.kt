package com.github.twobiers.dungeonmonolith.game.aggregates.game.domain

class GameNotFoundException(override val message: String) : Exception(message) {
}
