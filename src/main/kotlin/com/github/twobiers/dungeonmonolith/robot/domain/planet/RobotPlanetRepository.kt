package com.github.twobiers.dungeonmonolith.robot.domain.planet

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface RobotPlanetRepository : CrudRepository<RobotPlanet, UUID> {
  @Query("select p.neighbours from RobotPlanet p where p.planetId = ?1")
  fun findNeighboursOfPlanet(planetId: UUID): List<RobotPlanet>

  @Query("select p from RobotPlanet p where p.gameWorldId = ?1")
  fun findPlanetsInGameworld(gameworldId: UUID): List<RobotPlanet>

  fun getRandomPlanetInGameworld(gameworldId: UUID): RobotPlanet = findPlanetsInGameworld(gameworldId).shuffled().first()
  fun getRandomPlanet(): RobotPlanet = findAll().shuffled().first()

  @Modifying
  @Query("delete from RobotPlanet p where p.gameWorldId = ?1")
  fun deleteAllPlanetsInGameworld(gameworldId: UUID)
}
