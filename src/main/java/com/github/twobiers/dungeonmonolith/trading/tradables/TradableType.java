package com.github.twobiers.dungeonmonolith.trading.tradables;

public enum TradableType {
    ITEM,
    RESTORATION,
    UPGRADE,
    RESOURCE
}
