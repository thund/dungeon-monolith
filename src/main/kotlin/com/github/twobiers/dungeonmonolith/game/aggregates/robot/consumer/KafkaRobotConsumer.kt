package com.github.twobiers.dungeonmonolith.game.aggregates.robot.consumer

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.services.RobotService
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.consumer.dtos.RobotSpawnedDto
import com.github.twobiers.dungeonmonolith.game.aggregates.robot.consumer.dtos.RobotAttackedDto
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

@Component
class KafkaRobotConsumer @Autowired constructor(
    private val robotService: RobotService,
    private val objectMapper: ObjectMapper
) {

    @KafkaListener(topics = ["robot.integration"], groupId = "game", autoStartup = "true")
    fun robotListener(@Header("type") type: String, record: ConsumerRecord<String, String>) {
        when(type) {
            "RobotAttackedIntegrationEvent" -> handleRobotAttackedIntegrationEvent(record)
            "RobotSpawnedIntegrationEvent" -> handleRobotSpawned(record)
        }
    }

    private fun handleRobotAttackedIntegrationEvent(record: ConsumerRecord<String, String>) {
        val event = objectMapper.readValue(record.value(), RobotAttackedDto::class.java)

        if(!event.attacker.alive) {
            robotService.destroyRobot(event.attacker.robotId)
        }
        if(!event.target.alive) {
            robotService.destroyRobot(event.target.robotId)
        }
    }

    private fun handleRobotSpawned(record: ConsumerRecord<String, String>) {
        val event = objectMapper.readValue(record.value(), RobotSpawnedDto::class.java)

        robotService.newRobot(event.robot.id, event.robot.player)
    }
}
