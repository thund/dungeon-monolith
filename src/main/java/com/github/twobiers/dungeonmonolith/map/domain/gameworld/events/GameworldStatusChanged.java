package com.github.twobiers.dungeonmonolith.map.domain.gameworld.events;

import com.github.twobiers.dungeonmonolith.map.domain.gameworld.GameworldStatus;
import java.util.UUID;

public record GameworldStatusChanged(
    UUID id,
    GameworldStatus status
) {

}
