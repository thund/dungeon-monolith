package com.github.twobiers.dungeonmonolith.robot.application.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.twobiers.dungeonmonolith.robot.domain.robot.event.RobotDomainEvent
import mu.KotlinLogging
import org.apache.kafka.clients.producer.ProducerRecord
import org.springframework.context.event.EventListener
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

@Component
@Deprecated("We shouldn't publish domain events to Kafka anymore")
class KafkaPublishingDomainEventListener(
  val kafkaTemplate: KafkaTemplate<String, String>,
  val objectMapper: ObjectMapper
) {
  private val dateTimeFormatter = DateTimeFormatter.ISO_INSTANT
  private val logger = KotlinLogging.logger { }

  companion object {
    private const val robotTopic = "robot"
  }

  @EventListener(RobotDomainEvent::class)
  fun publishEventToKafka(robotDomainEvent: RobotDomainEvent) {

    val record = buildProducerRecord(
      robotDomainEvent,
      robotTopic
    )
    kafkaTemplate.send(record)
  }

  private fun buildProducerRecord(
    event: RobotDomainEvent,
    topic: String
  ): ProducerRecord<String, String> {
    val record =
      ProducerRecord(topic, event.robotId().toString(), objectMapper.writeValueAsString(event))
    record.headers().add("eventId", UUID.randomUUID().toString().encodeToByteArray())
    record.headers().add("type", event::class.simpleName.toString().encodeToByteArray())
    record.headers().add("version", "1".encodeToByteArray())
    record.headers().add("timestamp", dateTimeFormatter.format(Instant.now()).encodeToByteArray())
    event.playerIds().forEach { record.headers().add("playerId", it.toString().encodeToByteArray()) }
    return record
  }
}
