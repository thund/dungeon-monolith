package com.github.twobiers.dungeonmonolith.robot.application.robot.handlers

import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotIntegrationEventPublisher
import com.github.twobiers.dungeonmonolith.robot.application.robot.event.RobotUpgradedIntegrationEvent
import com.github.twobiers.dungeonmonolith.robot.domain.robot.RobotRepository
import com.github.twobiers.dungeonmonolith.robot.domain.robot.UpgradeType
import mu.KotlinLogging
import org.springframework.stereotype.Service
import java.util.*

@Service
class UpgradeRobotHandler(
  val robotRepository: RobotRepository,
  val integrationEventPublisher: RobotIntegrationEventPublisher
) {
  companion object {
    val logger = KotlinLogging.logger {}
  }

  fun handle(robotId: UUID, upgradeType: UpgradeType, level: Int, transactionId: UUID? = null) {
    val robot = robotRepository.findByIdOrThrow(robotId)
    robot.upgrade(upgradeType, level)
    logger.info("Successfully upgraded $upgradeType of robot $robotId")
    this.robotRepository.save(robot)

    val event = RobotUpgradedIntegrationEvent.build(robot, upgradeType, level)
    this.integrationEventPublisher.publish(event, transactionId)
  }
}
